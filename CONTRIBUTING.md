# Contributing to `Venus` master branch

### Bugs & Issues

Please submit any bugs you encounter when using the library to our [Github Issues Tracker](https://github.com/joseorihuela/venus/issues).

When submiting a bug report, please include a set of steps to reproduce the issue and any related information, browser, OS etc. If we can't see the issue then it will make solving things much more difficult.

Please create a fork of this [jsfiddle](http://jsfiddle.net/) to demonstrate bugs.

### Pull Requests

Anyone can jump on the issues board and grab off bugs to fix. This is probably the best way to become a contributor to Venus. Be sure to adhere to the style guides when submitting code.

* [Create a Pull Request](https://github.com/joseorihuela/venus/compare)
* [View Open Issues](https://github.com/joseorihuela/venus/issues)
