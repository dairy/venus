const $ = require('jquery'),
      ko = require('knockout');

require('jquery-ui/draggable');

ko.bindingHandlers.draggable = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        let $element = $(element);

        $element.draggable({ revert: true, cursor: 'move', grid: [ 1, 1 ], containment: '.shell-app-container', scroll: false, start: function () { $(this).css('margin', 0); } });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever any observables/computeds that are accessed change
        // Update the DOM element based on the supplied values here.
    }
};
