const $ = require('jquery'),
      ko = require('knockout');

ko.bindingHandlers.delegatedEvent = {
    init(element, valueAccessor, allBindings, viewModel, bindingContext) {
        let $element = $(element);

        $element.on(allBindings.get('event'), allBindings.get('selector'), function (event) {
            event.preventDefault();

            $(this).addClass('active').siblings().removeClass('active');

            valueAccessor()(ko.dataFor(this), event);
        });
    },
    update(element, valueAccessor, allBindings, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever any observables/computeds that are accessed change
        // Update the DOM element based on the supplied values here.
    }
};
