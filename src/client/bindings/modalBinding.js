const $ = require('jquery'),
      ko = require('knockout');

ko.bindingHandlers.modal = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        let $element = $(element);

        viewModel.visible.subscribe(function () {
            if (this._target()) {
                $element.foundation('reveal', 'open');
            } else {
                $element.foundation('reveal', 'close');
            }
        });
        $element.on('close.fndtn.reveal', function () {
            viewModel.destroy();
        });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever any observables/computeds that are accessed change
        // Update the DOM element based on the supplied values here.
    }
};
