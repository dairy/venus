const $ = require('jquery'),
      ko = require('knockout');

ko.bindingHandlers.counterAnim = {
    init: function (element, valueAccessor) {
        let value = ko.unwrap(valueAccessor());
        if (value < 10) {
            value = '0' + value;
        }
        $(element).html(ko.unwrap(valueAccessor()));
    },

    update: function (element, valueAccessor) {
        let value = ko.unwrap(valueAccessor()),
            $element = $(element);
        if (value < 10) {
            value = '0' + value;
        }
        $element.html(value);
        $element.effect('slide', {
            direction: 'down'
        });
    }
};
