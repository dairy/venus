const $ = require('jquery'),
      ko = require('knockout');

require('datepicker/datepicker.js');

ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindings) {
        let $element = $(element);

        $element.datepicker({
            numberOfMonths: 1,
            nextText: '',
            prevText: '',
            onSelect: function (dateText, inst) {
                allBindings().value(dateText);
            }
        });
    }
};
