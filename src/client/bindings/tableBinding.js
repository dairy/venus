const $ = require('jquery'),
    ko = require('knockout'),
    _ = require('lodash');

require('floatthead');

ko.bindingHandlers.table = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        let $element = $(element);

        $element.on('click', function (event) {
            let dataContext = ko.contextFor(event.target),
                selectFunc = _.get(_.find(dataContext.$parents, 'onSelect'), 'onSelect');

            if (selectFunc) {
                selectFunc(dataContext.$data, dataContext.$parent);
            }
        });
        $(document).ready(function () {
            $.each($element.find('th'), function () {
                let $this = $(this),
                    tableMap = allBindings().with.tableMap();

                _.findKey(tableMap, column => {
                    if (column.name === $this.text()) {
                        if (column.sortable) {
                            $this.append('<span class="sort"></span>');
                        }
                    }
                });
            });
            $element.find('th').on('click', function (event) {
                let $this = $(this),
                    sortOrderData = 'sortOrder',
                    sortOrder = $this.data(sortOrderData),
                    tableMap = allBindings().with.tableMap(),
                    $siblings = $this.siblings(),
                    column = tableMap[$this.attr('key')];

                if (column && column.sortable) {
                    $siblings.find('span').attr('class', 'sort');

                    if (sortOrder === undefined) {
                        $this.data(sortOrderData, 'ASC');
                        $this.find('span.sort').addClass('sortAsc');
                    } else if (sortOrder === 'ASC') {
                        $this.data(sortOrderData, 'DESC');
                        $this.find('span.sort').addClass('sortDesc');
                    } else if (sortOrder === 'DESC') {
                        $this.data(sortOrderData, 'ASC');
                        $this.find('span.sort').removeClass('sortDesc').addClass('sortAsc');
                    }

                    allBindings().with.sortColumn({
                        column: $this.text(),
                        attribute: column.attribute,
                        secondarySort: column.secondarySort,
                        sortOrder: $this.data(sortOrderData)
                    });
                }
            });
        });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        let $element = $(element);

        // not implemented here because it appears the dom elements for the row are not present yet
        // if (allBindings().with && allBindings().with.data() && $element.find('.has-tip').size() > 0) {
            // $element.foundation('tooltip', 'reflow');
        // }
    }
};
