const $ = require('jquery'),
    ko = require('knockout');

ko.bindingHandlers.active = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        let $element = $(element),
            activeClass = 'active';

        $element.on('click', function (event) {
            var $target = $(event.target),
                $parent = $target.parent(),
                $siblings = $parent.siblings();

            $siblings.removeClass(activeClass);
            $parent.addClass(activeClass);
        });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever any observables/computeds that are accessed change
        // Update the DOM element based on the supplied values here.
    }
};
