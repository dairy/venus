const $ = require('jquery'),
      ko = require('knockout');

ko.bindingHandlers.contextMenu = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        let $element = $(element),
            $contextMenu = $('#context-menu');

        $element.on('click', function (event) {
            let $target = $(event.target),
                hasContext = $target.hasClass('has-context');

            if (hasContext) {
                event.preventDefault();
                event.stopPropagation();

                $contextMenu.css({
                    'position': 'absolute',
                    'top': $target.offset().top + 33 + $('#shell-header').scrollTop(),
                    'left': $target.offset().left + 1 + $('#shell-header').scrollLeft()
                }).hide().fadeIn(200);

                $contextMenu.data('active', true);
            }
        });

        // Hide $contextMenu on any scroll activity.
        $(document).on('mousewheel DOMMouseScroll', function () {
            $contextMenu.fadeOut(100);
        });

        // Hide $contextMenu on any window resize.
        $(window).on('resize', function () {
            $contextMenu.fadeOut(100);
        });

        $(document).on('click', function (event) {
            let $target = $(event.target),
                hasContext = $target.hasClass('has-context'),
                data = 'active',
                isActive = ($contextMenu.data(data) === undefined) ? true : $contextMenu.data(data);

            if (isActive && !hasContext) {
                $contextMenu.fadeOut(100);
                $contextMenu.data(data, !isActive);
            }
        });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever any observables/computeds that are accessed change
        // Update the DOM element based on the supplied values here.
    }
};
