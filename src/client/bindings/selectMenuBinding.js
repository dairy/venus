const $ = require('jquery'),
    ko = require('knockout'),
    _ = require('lodash');

require('restrapio-multiselect');

ko.bindingHandlers.selectMenu = {
    init: function (element, valueAccessor) {
        let $element = $(element),
            optionsCaption = ko.unwrap(valueAccessor()).optionsCaption,
            selectedOptions = ko.unwrap(valueAccessor()).selectedOptions;

        /*eslint-disable*/
        $element.SumoSelect({
            placeholder: optionsCaption,
            csvDispCount: 3,
            captionFormat: `{0} ${optionsCaption} Selected`,
            floatWidth: 0,
            forceCustomRendering: true,
            nativeOnDevice: ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod'],
            outputAsCSV : false,
            csvSepChar : ',',
            okCancelInMulti: false,
            triggerChangeCombined : false,
            selectAll : $element.attr('multiple') === 'multiple',
            selectAlltext : 'Select All'
        });
        /*eslint-enable*/
        $element[0].sumo.onOptClick = $element[0].sumo.onSelectedOption;
        $element[0].sumo.onSelectedOption = function (li) {
            this.onOptClick(li);
            li.click(function () {
                if (li.hasClass('selected')) {
                    if (_.isArray(selectedOptions())) {
                        selectedOptions.push({
                            id: li.data('val'),
                            name: li.find('label').text()
                        });
                    } else {
                        selectedOptions({
                            id: li.data('val'),
                            name: li.find('label').text()
                        });
                    }
                } else {
                    if (_.isArray(selectedOptions())) {
                        selectedOptions.remove(function (item) { return item.id === li.data('val'); });
                    } else {
                        selectedOptions(null);
                    }
                }
            });
        };
        // $element[0].sumo.reload = function () {
        //    // TODO make sure wrappered onSelectedOption gets redefined
        //    console.log('Wrapper Method not implemented');
        // };
    },
    update: function (element, valueAccessor) {
        let $element = $(element),
            options = ko.unwrap(ko.unwrap(valueAccessor()).options),
            selectedOptions = ko.unwrap(valueAccessor()).selectedOptions;

        // empty out option list before repopulating
        _.forEach($element[0].sumo.E[0].options, (item, index) => {
            $element[0].sumo.remove(0);
        });
        _.forEach(options, item => {
            $element[0].sumo.add(item.id, item.name);
        });

        // TODO initialize selected options from model.  For now, assume they need to be empty
        if (selectedOptions.removeAll) {
            selectedOptions.removeAll();
        }
        $element[0].sumo.selAllState();
    }
};
