const $ = require('jquery'),
      core = require('core'),
      Shell = require('shell');

require('bindings');

/* Create Application Global Namespace
 --------------------------------------------------------------------------- */
let venus = window.venus = window.venus || {};

/* Init Vendor Libraries
 --------------------------------------------------------------------------- */
// require('imports?$=jquery!imports?this=>window!imports?define=>false!foundation-sites');
require('script!foundation/js/vendor/modernizr.js');
require('foundation/js/foundation.min.js');


$(function () {
    $(document).foundation();

    // TODO: Move this to the shell-modal viewModels or bindings
    // $('#shell-modal').foundation('reveal', 'open');

    window.onbeforeunload = function () {
        if (core.shell.viewModel.dispatcher.viewModel.getCommandQueue().hasChanges()) {
            return 'Are you sure?  You have unsaved changes!';
        }
    };
});


/* Application Entry Point
 --------------------------------------------------------------------------- */
function init() {
    core.init()
        .done(function () {
            core.shell = new Shell('shell');
        })
        .fail(function () {
        });
}


/* Execute Application Entry Point
 --------------------------------------------------------------------------- */
init();
