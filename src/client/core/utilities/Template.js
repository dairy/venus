const $ = require('jquery');

require('imports?ko=knockout!knockout-fast-foreach');


class Template {
    constructor() {}

    /*
     * Creates a new html script-template using the specified template (if it doesnt already exist)
     * and will append this to the body element, ready for
     * knockout consumption
     * if the template already exists, it will be replaced with the provided text. (to support theme overrides)
     *
     * @templateText - this is the html template to be turned into a script-template
     * @templateId - the uniqueId for the script tag that's holding the template*
     *
     */
    inject(template, id) {
        let $template = $(`#${id}`);

        if ($template.length === 0) {
            // Add new template placeholder
            $('<script type="text/html" id=`${id}`>`${template}`</script>').appendTo('body');
        } else {
            // Inject into existing placeholder
            $template.html(template);
        }
    }

    injectAppend(template, id, appendTo) {
        let $template = $(`#${id}`),
            $target = $(appendTo);

        if ($template.length === 0) {
            // Add new template placeholder
            $('<script type="text/html" id="`${id}`">`${template}`</script>').appendTo('body');
        } else {
            // Inject template appending to appendTo
            $template.html(template).appendTo($target);
        }
    }
}

module.exports = Template;
