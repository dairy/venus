const _ = require('lodash');

class Sorting {
    constructor() {}

    sortColumn(column, data) {
        let self = this;
        if (column.sortOrder === 'ASC') {
            data.sort(function (top, bottom) {
                let topValue = _.get(top, column.attribute),
                    bottomValue = _.get(bottom, column.attribute),
                    sortVal = topValue === bottomValue ? 0 : (topValue > bottomValue ? -1 : 1);
                if (sortVal === 0 && column.secondarySort) {
                    sortVal = self._sortBySecondary(column, top, bottom);
                }
                return sortVal;
            });
        } else if (column.sortOrder === 'DESC') {
            data.sort(function (top, bottom) {
                let topValue = _.get(top, column.attribute),
                    bottomValue = _.get(bottom, column.attribute),
                    sortVal = topValue === bottomValue ? 0 : (topValue > bottomValue ? 1 : -1);
                if (sortVal === 0 && column.secondarySort) {
                    sortVal = self._sortBySecondary(column, top, bottom);
                }
                return sortVal;
            });
        }
    }

    _sortBySecondary(column, top, bottom) {
        let topValue,
            bottomValue,
            i,
            sortVal = 0;
        for (i = 0; i < column.secondarySort.length; i++) {
            if (sortVal === 0) {
                topValue = _.get(top, column.secondarySort[i]);
                bottomValue = _.get(bottom, column.secondarySort[i]);
                sortVal = topValue === bottomValue ? 0 : (topValue > bottomValue ? 1 : -1);
            }
        }
        return sortVal;
    }
}

module.exports = Sorting;
