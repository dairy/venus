class PubSub {
    constructor() {
        this.subscribe = undefined;
        this.publish = undefined;
        this.unsubscribe = undefined;
    }
}

module.exports = PubSub;
