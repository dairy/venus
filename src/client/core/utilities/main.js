const i18n = require('i18next-client'),
      PubSub = require('./PubSub'),
      ko = require('knockout'),
      Template = require('./Template'),
      Sorter = require('./Sorter');

class Utilities {
    constructor() {
        this.i18n = i18n;
        this.pubsub = new PubSub();
        this.template = new Template();
        this.sorter = new Sorter();
    }
}

module.exports = Utilities;
