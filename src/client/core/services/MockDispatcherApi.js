const $ = require('jquery'),
      Settings = require('../Settings');

class MockDispatcherApi {
    constructor() {}

    dispatchable() {
        return {
            supply: function (commandBar) {
                return $.ajax({
                    url: 'mockSupplyData.json'
                });
            },
            demand: function (commandBar) {
                return $.ajax({
                    url: 'mockDemandData.json'
                });
            },
            transaction: function (commandBar) {
                return $.ajax({
                    url: 'mockTransactionData.json'
                });
            },
            validate: function (supply, demand) {
                return $.ajax({
                    url: 'mockValidateResponse.json'
                });
            },
        };
    }

    users() {
        return {
            current: function () {
                return {
                    session: function () {
                        return $.ajax({
                            url: 'mockUserSession.json'
                        });
                    }
                };
            }
        };
    }

    locations() {
        return {
            receiving: function () {
                return $.ajax({
                    url: 'mockReceivingLocationData.json'
                });
            },
            shipping: function () {
                return $.ajax({
                    url: 'mockShippingLocationData.json'
                });
            },
            balancing: function () {
                return $.ajax({
                    url: 'mockBalancingLocationData.json'
                });
            },
            storage: function () {
                return $.ajax({
                    url: 'mockStorageLocationData.json'
                });
            }
        };
    }

    customViews() {
        return {
            sell: function () {
                return $.ajax({
                    url: 'mockSellCustomViews.json'
                });
            },
            buy: function () {
                return $.ajax({
                    url: 'mockBuyCustomViews.json'
                });
            }
        };
    }

    contracts() {
        return {
            supply: function () {
                return $.ajax({
                    url: 'mockSupplyContracts.json'
                });
            },
            buy: function () {
                return $.ajax({
                    url: 'mockBuyContracts.json'
                });
            }
        };
    }

    contract() {
        return {
            partnerLocations: function (contract) {
                return $.ajax({
                    url: 'mockContractPartnerLocations.json'
                });
            }
        };
    }

    save(commandQueue) {
        return $.ajax({
            url: 'mockSaveResponse.json'
        });
    }
}

module.exports = MockDispatcherApi;
