const $ = require('jquery'),
      Settings = require('../Settings');

class DispatcherApi {
    constructor() {
        $.ajaxSetup({cache: false});
    }

    dispatchable() {
        return {
            supply: function (commandBar) {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.creamSupply.base,
                    { dispatchable: true, filter: commandBar.getFilterString() }
                );
            },
            demand: function (commandBar) {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.creamDemand.base,
                    { dispatchable: true, filter: commandBar.getFilterString() }
                );
            },
            transaction: function (commandBar) {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.creamTransaction.base,
                    { dispatchable: true, filter: commandBar.getFilterString() }
                );
            },
            validate: function (supply, demand) {
                var jqXHR, jsonData, settings, jsonResponse;
                jsonData = {'action': 'VALIDATE', 'itemId': supply.id, 'demandId': demand.id,
                        'itemReserved': supply.reserved(), 'demandReserved': demand.reserved()};
                settings = new Settings();
                jqXHR = $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    cache: false,
                    url: settings.webApiBaseUrl + settings.webApis.creamTransaction.base,
                    data: JSON.stringify(jsonData)
                });
                return jqXHR.promise();
            },
        };
    }

    users() {
        return {
            current: function () {
                return {
                    session: function () {
                        var settings = new Settings();
                        return $.getJSON(
                            settings.webApiBaseUrl + settings.webApis.users.base + settings.webApis.users.current.base +
                            settings.webApis.users.current.session
                        );
                    },
                    authorities: function () {
                        var settings = new Settings();
                        return $.getJSON(
                            settings.webApiBaseUrl + settings.webApis.users.base + settings.webApis.users.current.base +
                            settings.webApis.users.current.authorities
                        );
                    }
                };
            }
        };
    }

    locations() {
        return {
            receiving: function () {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.locations,
                    { receiving: true }
                );
            },
            shipping: function () {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.locations,
                    { shipping: true }
                );
            },
            balancing: function () {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.locations,
                    { balancing: true }
                );
            },
            storage: function () {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.locations,
                    { storage: true }
                );
            }
        };
    }

    customViews() {
        return {
            sell: function () {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.customViews,
                    { sell: true }
                );
            },
            buy: function () {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.customViews,
                    { buy: true }
                );
            }
        };
    }

    contracts() {
        return {
            supply: function () {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.contracts,
                    { supply: true }
                );
            },
            buy: function () {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.contracts,
                    { buy: true }
                );
            }
        };
    }

    contract() {
        return {
            partnerLocations: function (contract) {
                var settings = new Settings();
                return $.getJSON(
                    settings.webApiBaseUrl + settings.webApis.contracts + '/' + contract.id + '/partnerLocations'
                );
            }
        };
    }

    save(commandQueue) {
        var settings = new Settings();
        return $.ajax({
            method: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            traditional: true,
            url: settings.webApiBaseUrl + settings.webApis.creamDispatchable.base,
            data: JSON.stringify(commandQueue)
        });
    }
}

module.exports = DispatcherApi;
