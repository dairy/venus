const WebApi = require('./WebApi');

class Services {
    constructor() {
        this.webapi = new WebApi();
    }
}

module.exports = Services;
