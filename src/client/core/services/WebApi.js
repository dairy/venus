const DispatcherApi = require('./DispatcherApi'),
    MockDispatcherApi = require('./MockDispatcherApi'),
    Settings = require('../Settings');

class WebApi {
    constructor() {
        var settings = new Settings();
        if (settings.environment === 'local') {
            this.dispatcher = new MockDispatcherApi();
        } else {
            this.dispatcher = new DispatcherApi();
        }
    }
}

module.exports = WebApi;
