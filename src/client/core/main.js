const $ = require('jquery'),
      Settings = require('./Settings'),
      Security = require('./security'),
      Services = require('./services'),
      Utilities = require('./utilities');

/* Create Global Namespace
 --------------------------------------------------------------------------- */
/*eslint-disable*/
const venus = window.venus = window.venus || {};
/*eslint-enable*/


/* Core Initialization (Should this even be needed, maybe validate objects?
 --------------------------------------------------------------------------- */
function init() {
    let dfd = new $.Deferred();

    dfd.resolve();

    return dfd.promise();
}

venus.core = {
    init: init,
    security: new Security(),
    settings: new Settings(),
    services: new Services(),
    utilities: new Utilities()
};

module.exports = venus.core;
