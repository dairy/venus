const $ = require('jquery'),
      localSettings = require('../settings.local');

class Settings {
    constructor() {
        this.availableApps = {
            dispatcher: {
                appName: 'dispatcher',
                enabled: true
            }
        };

        this.debug = false;
        this.environment = 'dev';
        this.webApiBaseUrl = '../../api/';
        this.webApis = {};
        this.webApis.creamSupply = {base: 'cream/supplies'};
        this.webApis.creamDemand = {base: 'cream/demands'};
        this.webApis.creamTransaction = {base: 'cream/transactions'};
        this.webApis.creamDispatchable = {base: 'cream/dispatchables'};
        this.webApis.locations = 'locations';
        this.webApis.customViews = 'customviews';
        this.webApis.contracts = 'cream/contracts';
        this.webApis.users = {
            base: 'users',
            current: {
                base: '/current',
                session: '/session',
                authorities: '/authorities'
            }
        };

        // Digest localSettings if there are any
        $.extend(true, this, localSettings);
    }

    save() {
        if (this.debug) {
            // TODO: Add debug specific saves here
            return true;
        }
    }
}

module.exports = Settings;
