const AccessManager = require('./AccessManager');

class Security {
    constructor() {
        this.accessManager = new AccessManager();
    }
}

module.exports = Security;
