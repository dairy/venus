const $ = require('jquery'),
      ko = require('knockout'),
      ShellViewModel = require('./viewModels/ShellViewModel'),
      Growler = require('./components/growler/Growler'),
      ModalViewModel = require('./components/modal/viewModels/ModalViewModel');

/* Asset Entry Points
 --------------------------------------------------------------------------- */
require('app.scss');

class Shell {
    constructor(nodeId) {
        this.nodeId = nodeId;
        this.viewModel = new ShellViewModel({ nodeId: this.nodeId });
        this.growler = new Growler();
        this.modal = new ModalViewModel();

        ko.applyBindings(this.viewModel, document.getElementById(this.nodeId));
        ko.applyBindings(this.modal, document.getElementById('shell-modal'));
    }
}

module.exports = Shell;
