const core = require('core'),
      ko = require('knockout'),
      _ = require('lodash'),
      moment = require('moment'),
      shellViewTemplate = require('../views/shellView.tmpl.html'),
      HeaderViewModel = require('../components/header/viewModels/HeaderViewModel'),
      Dispatcher = require('../../apps/dispatcher/main');

class ShellViewModel {
    constructor(config) {
        var self = this;

        // Inject the HTML template.
        core.utilities.template.inject(shellViewTemplate, config.nodeId);

        this.dispatcher = new Dispatcher();
        this.header = ko.observable(new HeaderViewModel());
        this.currentSession = ko.observable();
        this.userAuthorities = ko.observableArray();
        this.balancingLocations = ko.observableArray();
        this.sellContracts = ko.observableArray();
        this.buyContracts = ko.observableArray();
        this.refreshServerSessionInfo();
        this.loading = ko.observable(false);
        this.currentSession.subscribe(function () {
            let session = this._target();

            if (session.regionName) {
                self.header().region(session.regionName);
            }
            if (session.startDate) {
                self.dispatcher.viewModel.commandBar.formattedStartDate(moment(session.startDate).format('M/D/YYYY'));
            }
            if (session.endDate) {
                self.dispatcher.viewModel.commandBar.formattedEndDate(moment(session.endDate).format('M/D/YYYY'));
            }
            self.dispatcher.viewModel.refreshAll();
        });
    }

    balancingLocations() {
        let self = this;
        return self.balancingLocations;
    }

    isCompleteAuthorized() {
        let self = this;
        return _.includes(self.userAuthorities(), 'brokerComplete');
    }

    isAssignToContractAuthorized() {
        let self = this;
        return _.includes(self.userAuthorities(), 'assignToContract');
    }

    refreshServerSessionInfo() {
        var self = this;
        core.services.webapi.dispatcher.users().current().session()
        .done(data => {
            self.currentSession(data);
        })
        .fail(data => {
            if (data.responseJSON === undefined) {
                let jsonObj = JSON.parse(data.responseText);
                if (jsonObj !== undefined) {
                    data.responseJSON = jsonObj;
                }
            }
            _.forEach(data.responseJSON, error => {
                if (error.severity === 'ERROR') {
                    core.shell.growler.error(error.message);
                } else if (error.severity === 'WARNING') {
                    core.shell.modal.message = error.message;
                    core.shell.growler.warning(error.message);
                }
            });
            if (data.responseJSON === undefined) {
                if (data.status !== 200) {
                    core.shell.growler.error(data.status + ': ' + data.statusText);
                }
            }
        });

        core.services.webapi.dispatcher.users().current().authorities()
        .done(authorities => {
            self.userAuthorities(authorities);
        });

        core.services.webapi.dispatcher.locations().balancing()
        .done(locations => {
            self.balancingLocations(locations);
        });
        core.services.webapi.dispatcher.contracts().buy()
        .done(contracts => {
            self.buyContracts(contracts);
        });
        core.services.webapi.dispatcher.contracts().supply()
        .done(contracts => {
            self.sellContracts(contracts);
        });
    }
}

module.exports = ShellViewModel;
