const ko = require('knockout'),
      MastheadViewModel = require('./MastheadViewModel'),
      NavigationViewModel = require('./NavigationViewModel'),
      CommandBarViewModel = require('./CommandBarViewModel');

class HeaderViewModel {
    constructor() {
        this.title = ko.observable('Dispatcher - Cream');
        this.region = ko.observable('Region');
        this.masthead = ko.observable(new MastheadViewModel());
        this.navigation = ko.observable(new NavigationViewModel([
            {
                name: 'CREAM',
                subNavItems: [
                    {
                        name: 'Manual Assignments'
                    }
                ]
            }
        ]));

        this.commandBar = ko.observable(new CommandBarViewModel());
    }
}

module.exports = HeaderViewModel;
