const ko = require('knockout'),
      moment = require('moment');

class CommandBarViewModel {
    constructor() {
        var m, day;
        this.contextMenu = {
            'copy': { name: 'Copy', callback: this.copy },
            'paste': { name: 'Paste', callback: this.paste }
        };

        this.startDate = moment().format('MM/DD/YYYY');
        m = moment();
        day = m.add(7, 'days');
        this.endDate = m.format('MM/DD/YYYY');
    }

    copy() {
        alert('Copy!');
    }

    paste() {
        alert('Paste!');
    }
}

module.exports = CommandBarViewModel;
