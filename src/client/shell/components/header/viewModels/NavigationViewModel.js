const ko = require('knockout'),
      _ = require('lodash'),
      NavItemModel = require('../models/NavItemModel');

class NavigationViewModel {
    constructor(items) {
        this.navItems = ko.observableArray();
        this.activeNavItem = ko.observable();

        _.forEach(items, (item) => {
            this.navItems.push(new NavItemModel(item));
        });

        this.activeNavItem(this.navItems()[0]);

        // Bind knockout callbacks to current 'this' context.
        this.addNavItem = _.bind(this.addNavItem, this);
        this.setActive = _.bind(this.setActive, this);
    }

    addNavItem(item) {
        if (item) {
            this.navItems.push(new NavItemModel({ name: item.name, subNavItems: item.subNavItems }));
        }
    };

    setActive(item, event) {
        if (item instanceof NavItemModel) {
            item.isActive(true);
            this.activeNavItem(item);
        }
    };
}

module.exports = NavigationViewModel;
