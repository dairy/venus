const ko = require('knockout');

class SubNavItemModel {
    constructor(item) {
        this.name = ko.observable(item.name);
        this.isActive = ko.observable(false);
    }
}

module.exports = SubNavItemModel;
