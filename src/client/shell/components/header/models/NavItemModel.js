const ko = require('knockout'),
      _ = require('lodash'),
      SubNavItemModel = require('./SubNavItemModel');

class NavItemModel {
    constructor(item) {
        this.name = ko.observable(item.name);
        this.isActive = ko.observable(false);
        this.subNavItems = ko.observableArray();

        if (item.subNavItems) {
            _.forEach(item.subNavItems, (subNavItem) => {
                this.subNavItems.push(new SubNavItemModel(subNavItem));
            });
        }
    }
}

module.exports = NavItemModel;
