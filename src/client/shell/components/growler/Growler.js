const toastr = require('toastr'),
      _ = require('lodash');

class Growler {
    constructor() {
        toastr.options = {
            'debug': false,
            'newestOnTop': false,
            'positionClass': 'toast-top-right',
            'preventDuplicates': false,
            'onclick': null,
            'timeOut': '0',
            'extendedTimeOut': '1000',
            'showEasing': 'swing',
            'hideEasing': 'linear',
            'showMethod': 'fadeIn',
            'hideMethod': 'fadeOut'
        };
    }

    clear() {
        toastr.clear();
    }

    error(message) {
        toastr.error(message);
    }

    info(message) {
        toastr.info(message);
    }

    remove() {
        toastr.remove();
    }

    success(message) {
        toastr.success(message);
    }

    warning(message) {
        toastr.warning(message);
    }
}

module.exports = Growler;
