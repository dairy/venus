const overlayViewTemplate = require('../views/overlayView.tmpl.html');

class OverlayViewModel {
    constructor(config) {
        // Inject the HTML template.
        core.utilities.template.inject(overlayViewTemplate, config.nodeId);

        this.name = ko.observable(resource.module.name);
    }
}

module.exports = OverlayViewModel;
