const core = require('core'),
      ko = require('knockout'),
      _ = require('lodash'),
      modalView = require('../views/modalView.tmpl.html');

class ModalViewModel {
    constructor(config) {
        // Inject the HTML template.
        // core.utilities.template.inject(modalView, config.nodeId);

        // this.name = ko.observable(resource.module.name);
        var self = this;
        this.name = '';
        this.title = ko.observable('');
        this.message = ko.observable('');
        this.validationMessage = ko.observable(null);
        this.visible = ko.observable(false);
        this.primaryButton = ko.observable({ display: 'OK', action: function () { return true; }});
        this.secondaryButton = ko.observable({ display: 'Cancel', action: function () { return true; }});
    }

    create(data) {
        let self = this;
        _.each(_.keys(self), function (key) {
            if (data[key]) {
                if (_.isFunction(self[key])) {
                    self[key](data[key]);
                } else {
                    self[key] = data[key];
                }
            }
        });
        self.visible(true);
    }

    isValid() {
        return true;
    }

    destroy() {
        this.visible(false);
        this.validationMessage(null);
        core.shell.viewModel.dispatcher.viewModel.commandBar.searching(false);
    }

    handlePrimary() {
        if (this.primaryButton().action(this)) {
            this.destroy();
        }
    }

    handleSecondary() {
        if (this.secondaryButton().action(this)) {
            this.destroy();
        }
    }
}

module.exports = ModalViewModel;
