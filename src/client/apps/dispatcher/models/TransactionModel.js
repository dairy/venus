const core = require('core'),
      _ = require('lodash'),
      ko = require('knockout'),
      moment = require('moment'),
      SupplyModel = require('./SupplyModel'),
      DemandModel = require('./DemandModel');

class TransactionModel {
    constructor(itemOrTxn, demand) {
        var self = this;
        if (demand === undefined) {
            this.mapTransaction(itemOrTxn);
        } else {
            this.createTransaction(itemOrTxn, demand);
        }

        this.dispatchableType = function (availability) {
            var displayClass;
            if (availability.displayName === 'External') {
                displayClass = 'dispatchable-type-external';
            } else if (availability.displayName === 'Internal') {
                displayClass = 'dispatchable-type-internal';
            } else {
                displayClass = 'dispatchable-type-contract';
            }
            return displayClass;
        };
        this.isModified = ko.computed(function () {
            return !_.isEmpty(self.status());
        });
        this.contractAssignment = ko.pureComputed(function () {
            if (self.supply.contractAssignment) {
                return self.supply.contractAssignment;
            } else if (self.demand.contractAssignment) {
                return self.demand.contractAssignment;
            }
        });
        this.balancingAssignment = ko.pureComputed(function () {
            if (self.supply.balancingAssignment) {
                return self.supply.balancingAssignment;
            }
        });

        this.hasContextChoices = ko.observable(false);
        this.changedEvent = ko.observable(false).extend({notify: 'always'});
    }

    void() {
        if (this.isNew() || this.isCompleted()) {
            this.status.push('rollback');
        } else {
            this.status.push('voided');
        }
        return {supply: this.supply.original, demand: this.demand.original};
    }

    restoreVoided() {
        if (this.isVoided()) {
            if (_.includes(this.supply.status(), 'broken')) {
                this.supply.status.remove('broken');
            }
            if (_.includes(this.demand.status(), 'broken')) {
                this.demand.status.remove('broken');
            }
            this.status.removeAll();
        }
    }

    isVoided() {
        return _.includes(this.status(), 'voided');
    }

    isNew() {
        return _.includes(this.status(), 'new');
    }

    isCompleted() {
        return _.includes(this.status(), 'completed');
    }

    isRollback() {
        return _.includes(this.status(), 'rollback');
    }

    mapTransaction(txn) {
        this.status = ko.observableArray();
        this.id = txn.id;
        this.breakable = txn.breakable;
        this.linkable = txn.linkable;
        this.matchAvailability = txn.matchAvailability;
        this.reservable = txn.reservable;
        this.txLock = txn.txLock;
        this.supply = this.mapSupply(txn, {
            id: txn.ltNumber,
            availability: txn.item.availability,
            supplyCompany: txn.item.company,
            comment: txn.item.comment,
            original: txn.item,
            txLock: txn.item.txLock,
            details: txn.supplySideDetails
        });
        this.demand = this.mapDemand(txn, {
            id: txn.demand.id,
            availability: txn.demand.availability,
            demandCompany: txn.demand.company,
            comment: txn.demand.comment,
            original: txn.demand,
            txLock: txn.demand.txLock,
            details: txn.demandSideDetails
        });
    }


    mapSupply(data, variableProps) {
        return _.extend(new SupplyModel({
            supplyCompany: data.supplyCompany,
            hauler: data.hauler,
            ltNumber: data.ltNumber,
            shippingDateTime: data.shippingDateTime,
            shippingLocation: data.shippingLocation,
            weight: data.weight,
            contractAssignment: data.contractAssignment,
            balancingAssignment: data.balancingAssignment
        }), variableProps);
    }

    mapDemand(data, variableProps) {
        return _.extend(new DemandModel({
            availability: data.availability,
            demandCompany: data.demandCompany,
            receivingDateTimeMax: data.receivingDateTimeMax,
            receivingDateTimeMin: data.receivingDateTimeMin,
            receivingLocation: data.receivingLocation,
            receivingMinDate: data.receivingMinDate,
            weight: data.weight,
            contractAssignment: data.contractAssignment
        }), variableProps);
    }

    createTransaction(supply, demand) {
        if (supply.isCompleted() || demand.isCompleted()) {
            this.status = ko.observableArray(['completed']);
        } else {
            this.status = ko.observableArray(['new']);
        }
        this.id = null;
        this.breakable = true;
        this.linkable = false;
        this.matchAvailability = demand.availability; // TODO
        this.reservable = false;
        this.txLock = null;
        this.supply = this.mapSupply(supply, {
            id: supply.id,
            availability: supply.availability,
            comment: supply.comment,
            details: supply.details,
            original: supply,
            txLock: supply.txLock
        });
        if (supply.isReserved()) {
            this.supply.status = ko.observableArray(['reserved']);
        }
        if (supply.isUnreserved()) {
            this.supply.status = ko.observableArray(['unreserved']);
        }
        this.demand = this.mapDemand(demand, {
            id: demand.id,
            availability: demand.availability,
            comment: demand.comment,
            details: demand.details,
            original: demand,
            txLock: demand.txLock
        });
        if (demand.isReserved()) {
            this.demand.status = ko.observableArray(['reserved']);
        }
        if (demand.isUnreserved()) {
            this.demand.status = ko.observableArray(['unreserved']);
        }
    }
}

module.exports = TransactionModel;
