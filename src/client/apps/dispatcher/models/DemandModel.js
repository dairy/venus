const core = require('core'),
      _ = require('lodash'),
      ko = require('knockout'),
      moment = require('moment');

class DemandModel {
    constructor(item) {
        var self = this;
        this.status = ko.observableArray();
        this.availability = item.availability;
        this.breakable = item.breakable;
        this.comment = item.comment;
        this.contractAssignment = item.contractAssignment;
        this.deleteable = item.deleteable;
        this.demandCompany = item.demandCompany;
        if (item.details) {
            this.details = item.details;
        }
        this.flippable = item.flippable;
        this.id = item.id;
        this.linkable = item.linkable;
        this.receivingDateTimeMax = item.receivingDateTimeMax;
        this.receivingDateTimeMin = item.receivingDateTimeMin;
        this.receivingLocation = item.receivingLocation;
        this.receivingMinDate = item.receivingMinDate;
        this.region = item.region;
        this.reservable = item.reservable;
        this.reserved = ko.observable(item.reserved);
        this.txLock = item.txLock;
        this.weight = item.weight;

        // TODO: Move this into a utility function for dispatcher
        this.dispatchableType = ko.pureComputed(function () {
            var displayClass;
            if (self.availability.displayName === 'External') {
                displayClass = 'dispatchable-type-external';
            } else if (self.availability.displayName === 'Internal') {
                displayClass = 'dispatchable-type-internal';
            } else {
                displayClass = 'dispatchable-type-contract';
            }
            return displayClass;
        });
        this.dispatchableState = ko.pureComputed(function () {
            if (self.reserved()) {
                return 'dispatchable-state-reserved';
            } else if (self.isFlippedExternal()) {
                return 'dispatchable-state-spot';
            } else if (self.isFlippedContract()) {
                return 'dispatchable-state-contract';
            }
        });
        this.formattedReceivingDateTime = ko.pureComputed(function () {
            return moment(self.receivingDateTimeMin).format('ddd M/D/YYYY H:mm') + ' - ' + moment(self.receivingDateTimeMax).format('H:mm');
        });
        this.isModified = ko.computed(function () {
            return !_.isEmpty(self.status());
        });
        this.isDeleted = ko.computed(function () {
            return _.includes(self.status(), 'deleted');
        });

        this.hasContextChoices = ko.observable(false);
        this.changedEvent = ko.observable(false).extend({notify: 'always'});
    }

    delete() {
        this.status.push('deleted');
    }

    undelete() {
        this.status.remove('deleted');
    }

    reserve() {
        if (_.includes(this.status(), 'unreserved')) {
            this.status.remove('unreserved');
        } else {
            this.status.push('reserved');
        }
        this.reserved(true);
    }

    unreserve() {
        if (_.includes(this.status(), 'reserved')) {
            this.status.remove('reserved');
        } else {
            this.status.push('unreserved');
        }
        this.reserved(false);
        this.reservable = true;
    }

    complete() {
        if (_.includes(this.status(), 'completed')) {
            this.status.remove('completed');
        } else {
            this.status.push('completed');
        }
    }

    assignToContract() {
        var demandModelContext = this,
        contracts = core.shell.viewModel.sellContracts();
        core.shell.viewModel.dispatcher.contractModal.create({
            title: 'Assign To Contract',
            message: 'Choose Contract Details',
            contracts: contracts,
            showClassifications: false,
            classifications: core.shell.viewModel.dispatcher.contractModal.classifications(),
            primaryButton: {
                display: 'Done',
                action: function (context) {
                    let retval = true;

                    if (context.isValid()) {
                        demandModelContext.contractAssignment = {
                            contract: context.contract(),
                            location: context.location(),
                            classification: context.classification()
                        };
                        demandModelContext.status.push('assignedToContract');
                    } else {
                        context.validationMessage('All selections are required');
                        retval = false;
                    }
                    return retval;
                }
            }
        });
    }

    isReserved() {
        return _.includes(this.status(), 'reserved');
    }

    isUnreserved() {
        return _.includes(this.status(), 'unreserved');
    }

    isAssignedToContract() {
        return _.includes(this.status(), 'assignedToContract');
    }

    isCompleted() {
        return _.includes(this.status(), 'completed');
    }

    isFlippedExternal() {
        return false;
    }

    isFlippedContract() {
        return false;
    }

    hasComment() {
        return this.comment !== null;
    }
}

module.exports = DemandModel;
