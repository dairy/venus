const core = require('core'),
      _ = require('lodash'),
      ko = require('knockout'),
      moment = require('moment');

class SupplyModel {
    constructor(item) {
        var self = this;
        this.status = ko.observableArray();
        this.availability = item.availability;
        this.balancingAssignment = item.balancingAssignment;
        this.breakable = item.breakable;
        this.comment = item.comment;
        this.contractAssignment = item.contractAssignment;
        this.deleteable = item.deleteable;
        if (item.details) {
            this.details = item.details;
        }
        this.flippable = item.flippable;
        this.id = item.id;
        this.linkable = item.linkable;
        this.priceFo1 = item.priceSellClass1;
        this.priceFo2 = item.priceSellClass2;
        this.priceFo3 = item.priceSellClass3;
        this.priceFo4 = item.priceSellClass4;
        this.priceCa1 = item.priceSellClass1Ca;
        this.priceCa2 = item.priceSellClass2Ca;
        this.priceCa3 = item.priceSellClass3Ca;
        this.priceCa4a = item.priceSellClass4aCa;
        this.priceCa4b = item.priceSellClass4bCa;
        this.region = item.region;
        this.reservable = item.reservable;
        this.reserved = ko.observable(item.reserved);
        this.shippingDateTime = item.shippingDateTime;
        this.shippingLocation = item.shippingLocation;
        this.supplyCompany = item.supplyCompany;
        this.txLock = item.txLock;
        this.weight = item.weight;
        // TODO: Move this into a utility function for dispatcher
        this.dispatchableType = ko.pureComputed(function () {
            var displayClass;
            if (self.availability.displayName === 'External') {
                displayClass = 'dispatchable-type-external';
            } else if (self.availability.displayName === 'Internal') {
                displayClass = 'dispatchable-type-internal';
            } else {
                displayClass = 'dispatchable-type-contract';
            }
            return displayClass;
        });
        this.dispatchableState = ko.pureComputed(function () {
            if (self.reserved()) {
                return 'dispatchable-state-reserved';
            } else if (self.isFlippedExternal()) {
                return 'dispatchable-state-spot';
            } else if (self.isFlippedContract()) {
                return 'dispatchable-state-contract';
            }
        });
        this.formattedShippingDateTime = ko.pureComputed(function () {
            return moment(self.shippingDateTime).format('ddd M/D/YYYY H:mm');
        });
        this.isModified = ko.computed(function () {
            return !_.isEmpty(self.status());
        });
        this.isDeleted = ko.computed(function () {
            return _.includes(self.status(), 'deleted');
        });

        this.hasContextChoices = ko.observable(false);
        this.changedEvent = ko.observable(false).extend({notify: 'always'});
    }

    delete() {
        this.status.push('deleted');
    }

    undelete() {
        this.status.remove('deleted');
    }

    reserve() {
        if (_.includes(this.status(), 'unreserved')) {
            this.status.remove('unreserved');
        } else {
            this.status.push('reserved');
        }
        this.reserved(true);
    }

    unreserve() {
        if (_.includes(this.status(), 'reserved')) {
            this.status.remove('reserved');
        } else {
            this.status.push('unreserved');
        }
        this.reserved(false);
        this.reservable = true;
    }

    complete() {
        if (_.includes(this.status(), 'completed')) {
            this.status.remove('completed');
        } else {
            this.status.push('completed');
        }
    }
    assignToContract() {
        var supplyModelContext = this,
        contracts = core.shell.viewModel.buyContracts();

        core.shell.viewModel.dispatcher.contractModal.create({
            title: 'Assign To Contract',
            message: 'Choose Contract Details:',
            contracts: contracts,
            classifications: core.shell.viewModel.dispatcher.contractModal.classifications(),
            primaryButton: {
                display: 'Done',
                action: function (context) {
                    let retval = true;

                    if (context.isValid()) {
                        supplyModelContext.contractAssignment = {
                            contract: context.contract(),
                            location: context.location(),
                            classification: context.classification()
                        };
                        supplyModelContext.status.push('assignedToContract');
                    } else {
                        context.validationMessage('All selections are required');
                        retval = false;
                    }
                    return retval;
                }
            }
        });
    }

    assignToBalancing() {
        var supplyModelContext = this;
        core.shell.viewModel.dispatcher.balancingModal.create({
            title: 'Assign To Balancing Plant',
            message: 'Complete all fields:',
            isInternalSupply: supplyModelContext.isInternal(),
            locations: core.shell.viewModel.balancingLocations(),
            classifications: core.shell.viewModel.dispatcher.balancingModal.classifications(),
            receivingDateMin: moment(supplyModelContext.shippingDateTime).format('M/D/YYYY'),
            receivingDateHourMin: moment(supplyModelContext.shippingDateTime).hour(),
            receivingDateMinuteMin: moment(supplyModelContext.shippingDateTime).minutes(),
            receivingDateMax: moment(supplyModelContext.shippingDateTime).format('M/D/YYYY'),
            receivingDateHourMax: moment(supplyModelContext.shippingDateTime).hour(),
            receivingDateMinuteMax: moment(supplyModelContext.shippingDateTime).minutes(),
            priceFo1: supplyModelContext.priceFo1,
            priceFo2: supplyModelContext.priceFo2,
            priceFo3: supplyModelContext.priceFo3,
            priceFo4: supplyModelContext.priceFo4,
            priceCa1: supplyModelContext.priceCa1,
            priceCa2: supplyModelContext.priceCa2,
            priceCa3: supplyModelContext.priceCa3,
            priceCa4a: supplyModelContext.priceCa4a,
            priceCa4b: supplyModelContext.priceCa4b,
            primaryButton: {
                display: 'Done',
                action: function (context) {
                    let retval = true;
                    if (context.isValid()) {
                        supplyModelContext.balancingAssignment = {
                            location: context.location(),
                            classification: context.classification(),
                            price: context.price(),
                            receivingDateTimeMin: context.receivingDateTimeMin,
                            receivingDateTimeMax: context.receivingDateTimeMax
                        };
                        if (supplyModelContext.isReserved()) {
                            supplyModelContext.reserved(false);
                            supplyModelContext.status.replace('reserved', 'assignedToBalancing');
                        } else {
                            supplyModelContext.status.push('assignedToBalancing');
                        }
                    } else {
                        context.validationMessage('The following errors have occurred:');
                        retval = false;
                    }
                    return retval;
                }
            }
        });
    }

    isReserved() {
        return _.includes(this.status(), 'reserved');
    }

    isUnreserved() {
        return _.includes(this.status(), 'unreserved');
    }

    isAssignedToContract() {
        return _.includes(this.status(), 'assignedToContract');
    }

    isAssignedToBalancing() {
        return _.includes(this.status(), 'assignedToBalancing');
    }

    isCompleted() {
        return _.includes(this.status(), 'completed');
    }

    isFlippedExternal() {
        return false;
    }

    isFlippedContract() {
        return false;
    }

    hasComment() {
        return this.comment !== null;
    }

    isInternal() {
        return this.supplyCompany.id === core.shell.viewModel.currentSession().companyId;
    }
}

module.exports = SupplyModel;
