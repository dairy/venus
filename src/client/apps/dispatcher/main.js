const ko = require('knockout'),
      DispatcherViewModel = require('./viewModels/DispatcherViewModel'),
      AssignToContractViewModel = require('./viewModels/AssignToContractViewModel'),
      AssignToBalancingViewModel = require('./viewModels/AssignToBalancingViewModel');


class Dispatcher {
    constructor() {
        this.viewModel = new DispatcherViewModel();
        this.contractModal = new AssignToContractViewModel();
        this.balancingModal = new AssignToBalancingViewModel();
        ko.applyBindings(this.viewModel, document.getElementById('dispatcher'));
        ko.applyBindings(this.viewModel.contextMenu, document.getElementById('context-menu'));
        ko.applyBindings(this.contractModal, document.getElementById('contract-modal'));
        ko.applyBindings(this.balancingModal, document.getElementById('balancing-modal'));
    }
}

module.exports = Dispatcher;
