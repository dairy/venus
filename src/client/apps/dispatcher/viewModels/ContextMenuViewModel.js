const core = require('core'),
      _ = require('lodash'),
      ko = require('knockout'),
      TransactionModel = require('../models/TransactionModel'),
      SupplyModel = require('../models/SupplyModel'),
      DemandModel = require('../models/DemandModel');

class ContextMenuViewModel {
    static get allChoices() {
        return [
            { key: 'assignTo', display: 'Assign to Contract', divider: false },
            { key: 'assignToBalancing', display: 'Assign to Balancing', divider: true },
            // { key: 'postContract', display: 'Post to Contract...', divider: false },
            // { key: 'postSpot', display: 'Post to Spot...', divider: true },
            { key: 'reserve', display: 'Reserve', divider: false },
            { key: 'unreserve', display: 'Unreserve', divider: false },
            { key: 'delete', display: 'Delete Load', divider: false },
            { key: 'undelete', display: 'Undelete Load', divider: false },
            { key: 'complete', display: 'Complete Assignment', divider: false },
            { key: 'break', display: 'Break', divider: false }
        ];
    }

    static getChoices(dispatchable) {
        var numberOfContracts = 0;
        if (dispatchable instanceof SupplyModel) {
            numberOfContracts = core.shell.viewModel.buyContracts().length;
        } else if (dispatchable instanceof DemandModel) {
            numberOfContracts = core.shell.viewModel.sellContracts().length;
        }

        return _.reject(ContextMenuViewModel.allChoices, function (choice) {
            return (choice.key === 'reserve' && (!dispatchable.reservable || dispatchable.reserved())) ||
                (choice.key === 'unreserve' && (!dispatchable.reserved || !dispatchable.reserved()) ) ||
                // (choice.key === 'postContract' && !dispatchable.flippable) ||
                // (choice.key === 'postSpot' && !dispatchable.flippable) ||
                (choice.key === 'break' && !dispatchable.breakable) ||
                (choice.key === 'delete' && (!dispatchable.deleteable || dispatchable.isDeleted())) ||
                (choice.key === 'undelete' && (!dispatchable.deleteable || !dispatchable.isDeleted())) ||
                (choice.key === 'assignTo' && (!dispatchable.reservable && (!dispatchable.reserved || !dispatchable.reserved()) || !core.shell.viewModel.isAssignToContractAuthorized() || numberOfContracts === 0)) ||
                (choice.key === 'assignToBalancing' && (!(dispatchable instanceof SupplyModel) || dispatchable.shippingLocation.isBrokerLocation || !(core.shell.viewModel.balancingLocations().length > 0))) ||
                (choice.key === 'complete' && (!dispatchable.reserved || !dispatchable.reserved() || !core.shell.viewModel.isCompleteAuthorized()));
        });
    }

    constructor() {
        _.extend(this, {
            choices: ko.observableArray(),
            currentItem: ko.observable(),

            // Bind knockout callbacks to current 'this' context.
            selectContextMenuItem: _.bind(this.selectContextMenuItem, this),
            initChoices: _.bind(this.initChoices, this)
        });
    }
    initChoices(dispatchable) {
        this.choices(ContextMenuViewModel.getChoices(dispatchable));
        this.currentItem(dispatchable);
    }

    selectContextMenuItem(data) {
        if (data.key === 'delete') {
            this.currentItem().delete();
        } else if (data.key === 'undelete') {
            this.currentItem().undelete();
        } else if (data.key === 'reserve') {
            this.currentItem().reserve();
        } else if (data.key === 'unreserve') {
            this.currentItem().unreserve();
        } else if (data.key === 'break') {
            this.currentItem().void();
        } else if (data.key === 'complete') {
            this.currentItem().complete();
        } else if (data.key === 'assignTo') {
            this.currentItem().assignToContract();
        } else if (data.key === 'assignToBalancing') {
            this.currentItem().assignToBalancing();
        }
    }
}

module.exports = ContextMenuViewModel;
