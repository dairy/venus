const core = require('core'),
      ko = require('knockout'),
      _ = require('lodash'),
      moment = require('moment'),
      $ = require('jquery'),
      TransactionModel = require('../models/TransactionModel'),
      SupplyModel = require('../models/SupplyModel'),
      DemandModel = require('../models/DemandModel'),
      ContextMenuViewModel = require('./ContextMenuViewModel');

class TransactionViewModel {
    constructor(filter) {
        _.extend(this, {
            data: ko.observableArray(),
            selectedSupply: ko.observable(),
            selectedDemand: ko.observable(),
            newSupplyQueue: ko.observableArray(),
            newDemandQueue: ko.observableArray(),
            tableMap: ko.observable({
            supplyType: {
                name: 'type',
                attribute: 'supply.availability.displayName',
                secondarySort: ['supply.shippingDateTime', 'supply.shippingLocation.name'],
                sortable: true
            },
            supplyShippingDateTime: {
                name: 'ship date',
                attribute: 'supply.shippingDateTime',
                secondarySort: ['supply.shippingLocation.name'],
                sortable: true
            },
            supplyLocation: {
                name: 'ship location',
                attribute: 'supply.shippingLocation.name',
                secondarySort: ['supply.shippingDateTime'],
                sortable: true
            },
            supplyLt: {
                name: 'lt#',
                attribute: 'supply.id',
                sortable: true
            },
            supplyWeight: {
                name: 'weight',
                attribute: 'supply.weight',
                sortable: false
            },
            demandType: {
                name: 'type',
                attribute: 'demand.availability.displayName',
                secondarySort: ['demand.receivingDateTimeMin', 'demand.receivingLocation.name'],
                sortable: true
            },
            demandReceivingDateTime: {
                name: 'receiving date',
                attribute: 'demand.receivingDateTimeMin',
                secondarySort: ['demand.receivingLocation.name'],
                sortable: true
            },
            demandLocation: {
                name: 'receiving location',
                attribute: 'demand.receivingLocation.name',
                secondarySort: ['demand.receivingDateTimeMin'],
                sortable: true
            },
            demandDmd: {
                name: 'dmd#',
                attribute: 'demand.id',
                sortable: true
            },
            demandWeight: {
                name: 'weight',
                attribute: 'demand.weight',
                sortable: false
            }
            }),

            // Bind knockout callbacks to current 'this' context.
            refresh: _.bind(this.refresh, this),
            onSelect: _.bind(this.onSelect, this),
            handleVoided: _.bind(this.handleVoided, this),
            sortColumn: _.bind(this.sortColumn, this),
            addTransaction: _.bind(this.addTransaction, this),
            prepareTransaction: _.bind(this.prepareTransaction, this),

            // TODO: Move this into a utility function for dispatcher
            dispatchableType: availability => {
                let displayClass;

                if (availability.displayName === 'External') {
                    displayClass = 'dispatchable-type-external';
                } else if (availability.displayName === 'Internal') {
                    displayClass = 'dispatchable-type-internal';
                } else {
                    displayClass = 'dispatchable-type-contract';
                }

                return displayClass;
            }
        });
    }

    loadingScreenOff() {
        core.shell.viewModel.loading(false);
    }

    refresh(filter) {
        let self = this;

        return core.services.webapi.dispatcher.dispatchable().transaction(filter)
            .done(data => {
                self.data(ko.utils.arrayMap(data, self.prepareTransaction));
            })
            .fail(data => {
                _.forEach(data.responseJSON, error => {
                    if (error.severity === 'ERROR') {
                        core.shell.growler.error(error.message);
                    } else if (error.severity === 'WARNING') {
                        // We shouldn't receive any warnings here.
                    }
                });
            });
    }

    onSelect(supplyOrDemand, transaction) {
        // Put a reference to the transaction on the object since we'll need it later
        supplyOrDemand.transaction = transaction;
        if (supplyOrDemand instanceof SupplyModel) {
            if (this.selectedSupply() === supplyOrDemand) {
                this.selectedSupply(null);
            } else {
                this.selectedSupply(supplyOrDemand);
            }
        } else if (supplyOrDemand instanceof DemandModel) {
            if (this.selectedDemand() === supplyOrDemand) {
                this.selectedDemand(null);
            } else {
                this.selectedDemand(supplyOrDemand);
            }
        }
    }
    isSupplySelected(supply) {
        return supply === this.selectedSupply();
    }

    isDemandSelected(demand) {
        return demand === this.selectedDemand();
    }

    handleVoided(transaction) {
        if (_.find(this.data(), function (txn) {
                return !txn.isVoided() && !txn.isRollback() && txn.supply.id === transaction.supply.id;
            }) === undefined && transaction.supply.original && transaction.supply.id && !transaction.supply.shippingLocation.isBrokerLocation) {
            transaction.supply.original.broken = true;
            this.newSupplyQueue.push(transaction.supply.original);
        }
        if (_.find(this.data(), function (txn) {
                return !txn.isVoided() && !txn.isRollback() && txn.demand.id === transaction.demand.id;
            }) === undefined && transaction.demand.original && transaction.demand.id && !transaction.demand.receivingLocation.isBrokerLocation) {
            transaction.demand.original.broken = true;
            this.newDemandQueue.push(transaction.demand.original);
        }
    }

    handleRollback(transaction) {
        if (_.includes(transaction.supply.original.status(), 'completed')) {
            transaction.supply.original.status.remove('completed');
        }
        if (_.includes(transaction.supply.original.status(), 'assignedToContract')) {
            transaction.supply.original.status.remove('assignedToContract');
            transaction.supply.original.contractAssignment = {};
        }
        if (_.includes(transaction.supply.original.status(), 'assignedToBalancing')) {
            transaction.supply.original.status.remove('assignedToBalancing');
            transaction.supply.original.balancingAssignment = {};
        }
        if (_.includes(transaction.demand.original.status(), 'completed')) {
            transaction.demand.original.status.remove('completed');
        }
        if (_.includes(transaction.demand.original.status(), 'assignedToContract')) {
            transaction.demand.original.status.remove('assignedToContract');
            transaction.demand.original.contractAssignment = {};
        }
        this.handleVoided(transaction);
        this.removeTransaction(transaction);
    }

    removeTransaction(transaction) {
        this.data.remove(transaction);
    }

    prepareTransaction(txnId) {
        let transaction = new TransactionModel(txnId);

        this.addStatusChangeHandler(transaction);
        transaction.hasContextChoices(ContextMenuViewModel.getChoices(transaction).length > 0);
        return transaction;
    }

    addTransaction(supply, demand) {
        let transaction = new TransactionModel(supply, demand),
            dataArray = this.data();

        transaction.hasContextChoices(ContextMenuViewModel.getChoices(transaction).length > 0);

        this.addStatusChangeHandler(transaction);
        dataArray.unshift(transaction);
        this.data.valueHasMutated();
        // Catch 'new' or 'complete' status from constructor
        transaction.changedEvent(true);
    }


    findVoided(supply, demand) {
        return _.find(this.data(), function (txn) {
            return txn.isVoided() && txn.supply.id === supply.id && txn.demand.id === demand.id;
        });
    }

    addStatusChangeHandler(transaction) {
        let self = this;

        transaction.status.subscribe(function () {
            if (transaction.isVoided()) {
                self.handleVoided(transaction);
            } else if (transaction.isRollback()) {
                self.handleRollback(transaction);
            }

            transaction.changedEvent(true);
        });
    }

    sortColumn(column) {
        core.utilities.sorter.sortColumn(column, this.data);
    }
}

module.exports = TransactionViewModel;
