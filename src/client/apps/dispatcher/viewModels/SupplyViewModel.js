const core = require('core'),
      _ = require('lodash'),
      ko = require('knockout'),
      $ = require('jquery'),
      moment = require('moment'),
      SupplyModel = require('../models/SupplyModel'),
      ContextMenuViewModel = require('./ContextMenuViewModel');

class SupplyViewModel {
    constructor(filter) {
        _.extend(this, {
            data: ko.observableArray(),
            selected: ko.observable(),
            newDemandQueue: ko.observableArray(),
            tableMap: ko.observable({
                type: {
                    name: 'type',
                    attribute: 'availability.displayName',
                    secondarySort: ['shippingDateTime', 'shippingLocation.name'],
                    sortable: true
                },
                shippingDateTime: {
                    name: 'date',
                    attribute: 'shippingDateTime',
                    secondarySort: ['shippingLocation.name'],
                    sortable: true
                },
                location: {
                    name: 'location',
                    attribute: 'shippingLocation.name',
                    secondarySort: ['shippingDateTime'],
                    sortable: true
                },
                lt: {
                    name: 'lt#',
                    attribute: 'id',
                    sortable: true
                },
                weight: {
                    name: 'weight',
                    attribute: 'weight',
                    sortable: false
                }
            }),

            // Bind knockout callbacks to current 'this' context.
            refresh: _.bind(this.refresh, this),
            onSelect: _.bind(this.onSelect, this),
            delete: _.bind(this.delete, this),
            prepareSupply: _.bind(this.prepareSupply, this),
            addSupply: _.bind(this.addSupply, this),
            sortColumn: _.bind(this.sortColumn, this)
        });
    }


    refresh(filter) {
        let self = this;

        return core.services.webapi.dispatcher.dispatchable().supply(filter)
        .done(data => {
            self.data(ko.utils.arrayMap(data, self.prepareSupply));
        })
        .fail(data => {
            _.forEach(data.responseJSON, error => {
                core.shell.growler.error(error.message);
            });
            if (data.responseJSON === undefined) {
                core.shell.growler.error('Search Results: ' + data.status + ': ' + data.statusText);
            }
        });
    }

    prepareSupply(item) {
        let supply,
            self = this;

        if (item instanceof SupplyModel) {
            supply = item;
        } else {
            supply = new SupplyModel(item);
            if (item.broken) {
                supply.status.push('broken');
            }

            supply.status.subscribe(function () {
                if (supply.isDeleted()) {
                    self.delete(supply);
                } else if (supply.isAssignedToBalancing()) {
                    self.assignToBalancing(supply);
                } else if (supply.isReserved() && !supply.isCompleted()) {
                    self.reserve(supply);
                } else if (supply.isAssignedToContract()) {
                    self.assignToContract(supply);
                } else if (supply.isCompleted()) {
                    self.complete(supply);
                } else if (supply.isUnreserved()) {
                    self.unreserve(supply);
                }

                supply.changedEvent(true);
            });
        }
        supply.hasContextChoices(ContextMenuViewModel.getChoices(supply).length > 0);
        return supply;
    }

    addSupply(item) {
        let supply = this.prepareSupply(item);
        this.data.push(supply);

        return supply;
    }

    removeSupply(item) {
        this.data.remove(item);
    }

    onSelect(item) {
        if (this.selected() === item) {
            this.selected(null);
        } else {
            this.selected(item);
        }
    }

    delete(item) {
        if (this.selected() === item) {
            this.selected(null);
        }
    }

    reserve(item) {

    }

    unreserve(item) {
        if (item.reservedBy) {
            this.newDemandQueue.push(item.reservedBy);
        }
    }

    complete(item) {
        if (this.selected() !== item) {
            this.selected(item);
        }
    }

    assignToContract(item) {
        if (this.selected() !== item) {
            this.selected(item);
        }
    }

    assignToBalancing(item) {
        if (this.selected() !== item) {
            this.selected(item);
        }
    }

    isSelected(supply) {
        return supply === this.selected();
    }

    sortColumn(column) {
        core.utilities.sorter.sortColumn(column, this.data);
    }
}

module.exports = SupplyViewModel;
