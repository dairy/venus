const core = require('core'),
      _ = require('lodash'),
      $ = require('jquery'),
      ko = require('knockout');

class AssignToContractViewModel {
    constructor() {
        var self = this;
        this.name = '';
        this.title = ko.observable('');
        this.message = ko.observable('');
        this.validationMessage = ko.observable(null);
        this.visible = ko.observable(false);
        this.contracts = ko.observableArray();
        this.locations = ko.observableArray();
        // TODO replace with enum from server
        this.classifications = ko.observableArray([
            {id: -1, name: '-- Select --'},
            {id: 0, name: 'FOI'},
            {id: 1, name: 'FOII'},
            {id: 2, name: 'FOIII'},
            {id: 3, name: 'FOIV'},
            {id: 4, name: 'CA1'},
            {id: 5, name: 'CA2'},
            {id: 6, name: 'CA3'},
            {id: 7, name: 'CA4a'},
            {id: 8, name: 'CA4b'}
        ]);
        this.contract = ko.observable();
        this.location = ko.observable();
        this.classification = ko.observable(this.classifications()[0]);
        this.showClassifications = ko.observable(true);
        this.primaryButton = ko.observable({ display: 'OK', action: function () { return true; }});
        this.secondaryButton = ko.observable({ display: 'Cancel', action: function () { return true; }});

        this.contract.subscribe(function () {
            if (this._target()) {
                core.services.webapi.dispatcher.contract().partnerLocations({id: this._target().id})
                .done(locations => {
                    self.locations(locations);
                });
            }
        });
    }

    classifications() {
        let self = this;
        return self.classifications;
    }

    create(data) {
        let self = this;
        _.each(_.keys(self), function (key) {
            if (_.has(data, key)) {
                if (_.isFunction(self[key])) {
                    self[key](data[key]);
                } else {
                    self[key] = data[key];
                }
            }
        });
        if (!self.showClassifications()) {
            // default it to whatever since we'll determine it server side
            self.classification(self.classifications()[1]);
        }
        self.visible(true);
    }

    isValid() {
        return this.contract() && this.location() && this.classification().id >= 0;
    }

    destroy() {
        this.visible(false);
        this.validationMessage(null);
        this.contracts(null);
        this.locations(null);
        this.contract(null);
        this.location(null);
        this.showClassifications(true);
        this.classification(this.classifications()[0]);
    }

    handlePrimary() {
        if (this.primaryButton().action(this)) {
            this.destroy();
        }
    }

    handleSecondary() {
        if (this.secondaryButton().action(this)) {
            this.destroy();
        }
    }
}

module.exports = AssignToContractViewModel;
