const core = require('core'),
      ko = require('knockout'),
      _ = require('lodash'),
      $ = require('jquery'),
      moment = require('moment'),
      SupplyViewModel = require('./SupplyViewModel'),
      DemandViewModel = require('./DemandViewModel'),
      TransactionViewModel = require('./TransactionViewModel'),
      TransactionModel = require('../models/TransactionModel'),
      CommandBarViewModel = require('./CommandBarViewModel'),
      ContextMenuViewModel = require('./ContextMenuViewModel');


class DispatcherViewModel {
    constructor() {
        let self = this;

        _.extend(this, {
            commandBar: new CommandBarViewModel(),
            refreshing: ko.observable(),
            contextMenu: new ContextMenuViewModel(),
            lastSaved: ko.observable(),

            // Bind knockout callbacks to current 'this' context.
            refreshAll: _.bind(this.refreshAll, this),
            assign: _.bind(this.assign, this),
            clearSelected: _.bind(this.clearSelected, this),
            save: _.bind(this.save, this),
            discard: _.bind(this.discard, this),
            initContextMenu: _.bind(this.initContextMenu, this)
        });

        this.supply = new SupplyViewModel(this.commandBar);
        this.supply.data.subscribe(function (changes) {
            _.forEach(changes, function (change) {
                if (change.status === 'added') {
                    change.value.changedEvent.subscribe(_.bind(self.updateUnsavedChanges, self));
                }
            });
        }, null, 'arrayChange');
        this.supplyChanges = ko.observable(0);

        this.demand = new DemandViewModel(this.commandBar);
        this.demand.data.subscribe(function (changes) {
            _.forEach(changes, function (change) {
                if (change.status === 'added') {
                    change.value.changedEvent.subscribe(_.bind(self.updateUnsavedChanges, self));
                }
            });
        }, null, 'arrayChange');
        this.demandChanges = ko.observable(0);

        this.transaction = new TransactionViewModel(this.commandBar);
        this.transaction.data.subscribe(function (changes) {
            _.forEach(changes, function (change) {
                if (change.status === 'added') {
                    change.value.changedEvent.subscribe(_.bind(self.updateUnsavedChanges, self));
                }
            });
        }, null, 'arrayChange');
        this.transactionChanges = ko.observable(0);

        this.hasChanges = ko.pureComputed(function () {
            return self.supplyChanges() + self.demandChanges() + self.transactionChanges() > 0;
        });

        this.saveInProgress = false;

        this.commandBar.searching.subscribe(function () {
            if (this._target()) {
                if (self.getCommandQueue().hasChanges()) {
                    core.shell.modal.create({
                        title: 'Are you sure?',
                        message: 'You have unsaved changes!  Hit OK to Discard the changes and continue.',
                        primaryButton: {
                            display: 'OK',
                            action: function (context) {
                                let retval = true;
                                self.refreshAll();
                                return retval;
                            }
                        },
                        secondaryButton: {
                            display: 'Cancel',
                            action: function (context) { self.commandBar.searching(false); return true; }
                        }
                    });
                } else {
                    self.refreshAll();
                }
            }
        });

        this.supply.selected.subscribe(function () {
            let supply = this._target(),
                demand = self.demand.selected(),
                transactedDemand = self.transaction.selectedDemand(),
                newDemand = null;

            if (supply) {
                self.transaction.selectedSupply(null);
            }
            if (supply && supply.contractAssignment) {
                newDemand = self.demand.addDemand({
                    availability: {displayName: 'Contract'},
                    breakable: false,
                    deleteable: false,
                    demandCompany: supply.contractAssignment.location.company,
                    details: 'Contract: ' + supply.contractAssignment.contract.name + '<br\>Location: ' + supply.contractAssignment.location.name,
                    flippable: false,
                    linkable: false,
                    receivingDateTimeMax: supply.shippingDateTime,
                    receivingDateTimeMin: supply.shippingDateTime,
                    receivingLocation: supply.contractAssignment.location,
                    receivingMinDate: supply.shippingDateTime,
                    region: supply.region,
                    reservable: false,
                    reserved: false,
                    weight: supply.weight
                });

                self.assign(supply, newDemand);
            } else if (supply && supply.isCompleted()) {
                newDemand = self.demand.addDemand({
                    availability: supply.availability,
                    breakable: false,
                    deleteable: false,
                    demandCompany: {name: 'Offline'},
                    details: 'Completed off the site',
                    flippable: false,
                    linkable: false,
                    receivingDateTimeMax: supply.shippingDateTime,
                    receivingDateTimeMin: supply.shippingDateTime,
                    receivingLocation: {name: 'Offline'},
                    receivingMinDate: supply.shippingDateTime,
                    region: supply.region,
                    reservable: false,
                    reserved: false,
                    weight: supply.weight
                });

                self.assign(supply, newDemand);
            } else if (supply && demand && self.isImplicitReservation(supply, demand)) {
                self.reserveIfValid(supply, demand)
                .fail(data => {
                    self.supply.selected(null);
                });
            } else if (supply && supply.balancingAssignment) {
                newDemand = self.demand.addDemand({
                    availability: {displayName: supply.availability.displayName},
                    breakable: false,
                    deleteable: false,
                    demandCompany: supply.balancingAssignment.location.company,
                    details: 'Balancing Plant: ' + supply.balancingAssignment.location.name,
                    flippable: false,
                    linkable: false,
                    receivingDateTimeMax: supply.balancingAssignment.receivingDateTimeMax,
                    receivingDateTimeMin: supply.balancingAssignment.receivingDateTimeMin,
                    receivingLocation: supply.balancingAssignment.location,
                    region: supply.region,
                    reservable: false,
                    reserved: false,
                    weight: supply.weight
                });

                self.assign(supply, newDemand);
            } else if (supply && demand) {
                self.assignIfValid(supply, demand)
                .fail(data => {
                    self.supply.selected(null);
                });
            } else if (supply && transactedDemand) {
                self.assignIfValid(supply, transactedDemand)
                .done(data => {
                    transactedDemand.transaction.void();
                })
                .fail(data => {
                    self.supply.selected(null);
                });
            }
        });

        this.demand.selected.subscribe(function () {
            let supply = self.supply.selected(),
                demand = this._target(),
                transactedSupply = self.transaction.selectedSupply(),
                newSupply = null;

            if (demand) {
                self.transaction.selectedDemand(null);
            }
            if (demand && demand.contractAssignment) {
                newSupply = self.supply.addSupply({
                    availability: {displayName: 'Contract'},
                    breakable: false,
                    deleteable: false,
                    supplyCompany: demand.contractAssignment.location.company,
                    details: 'Contract: ' + demand.contractAssignment.contract.name + '<br\>Location: ' + demand.contractAssignment.location.name,
                    flippable: false,
                    linkable: false,
                    shippingDateTime: demand.receivingDateTimeMin,
                    shippingLocation: demand.contractAssignment.location,
                    region: demand.region,
                    reservable: false,
                    reserved: false,
                    weight: demand.weight
                });

                self.assign(newSupply, demand);
            } else if (demand && demand.isCompleted()) {
                newSupply = self.supply.addSupply({
                    availability: demand.availability,
                    breakable: false,
                    deleteable: false,
                    supplyCompany: {name: 'Offline'},
                    details: 'Completed off the site',
                    flippable: false,
                    linkable: false,
                    shippingDateTime: demand.receivingDateTimeMin,
                    shippingLocation: {name: 'Offline'},
                    region: demand.region,
                    reservable: false,
                    reserved: false,
                    weight: demand.weight
                });

                self.assign(newSupply, demand);
            } else if (supply && demand && self.isImplicitReservation(supply, demand)) {
                self.reserveIfValid(supply, demand)
                .fail(data => {
                    self.demand.selected(null);
                });
            } else if (supply && demand) {
                self.assignIfValid(supply, demand)
                .fail(data => {
                    self.demand.selected(null);
                });
            } else if (transactedSupply && demand) {
                self.assignIfValid(transactedSupply, demand)
                .done(data => {
                    transactedSupply.transaction.void();
                })
                .fail(data => {
                    self.demand.selected(null);
                });
            }
        });

        this.transaction.selectedSupply.subscribe(function () {
            let transactedSupply = this._target(),
                demand = self.demand.selected(),
                transactedDemand = self.transaction.selectedDemand(),
                supply = null,
                originalItems = null;

            if (transactedSupply) {
                self.supply.selected(null);
            }
            if (transactedSupply && demand && self.isImplicitReservation(transactedSupply, demand)) {
                self.reserveIfValid(transactedSupply, demand)
                .fail(data => {
                    self.transaction.selectedSupply(null);
                });
            } else if (transactedSupply && demand) {
                // break and assign
                self.assignIfValid(supply, demand)
                .done(data => {
                    transactedSupply.transaction.void();
                })
                .fail(data => {
                    self.transaction.selectedSupply(null);
                });
            } else if (transactedSupply && transactedDemand) {
                if (transactedSupply.transaction === transactedDemand.transaction) {
                    self.transaction.selectedSupply(null);
                    core.shell.growler.error('Please choose a different Supply.');
                    return;
                }
                // break both
                self.assignIfValid(transactedSupply.original, transactedDemand.original)
                .done(data => {
                    transactedDemand.transaction.void();
                    transactedSupply.transaction.void();
                })
                .fail(data => {
                    self.transaction.selectedSupply(null);
                });
            }
        });

        this.transaction.selectedDemand.subscribe(function () {
            let transactedDemand = this._target(),
                supply = self.supply.selected(),
                transactedSupply = self.transaction.selectedSupply(),
                demand = null;

            if (transactedDemand) {
                self.demand.selected(null);
            }
            if (transactedDemand && supply && self.isImplicitReservation(supply, transactedDemand)) {
                self.reserveIfValid(supply, transactedDemand)
                .fail(data => {
                    self.transaction.selectedDemand(null);
                });
            } else if (transactedDemand && supply) {
                // break and assign
                self.assignIfValid(supply, transactedDemand.original)
                .done(data => {
                    transactedDemand.transaction.void();
                })
                .fail(data => {
                    self.transaction.selectedDemand(null);
                });
            } else if (transactedSupply && transactedDemand) {
                if (transactedSupply.transaction === transactedDemand.transaction) {
                    self.transaction.selectedDemand(null);
                    core.shell.growler.error('Please choose a different Demand.');
                    return;
                }
                // break both
                self.assignIfValid(transactedSupply.original, transactedDemand.original)
                .done(data => {
                    transactedDemand.transaction.void();
                    transactedSupply.transaction.void();
                })
                .fail(data => {
                    self.transaction.selectedDemand(null);
                });
            }
        });

        // Watch for new supplies resulting from breaking a transaction
        this.transaction.newSupplyQueue.subscribe(function () {
            let supplies = this._target();

            if (supplies.length > 0) {
                _.each(supplies, function (element, index, list) {
                    self.supply.addSupply(element);
                });
                this._target.removeAll();
            }
        });

        // Watch for new demands resulting from breaking a transaction
        this.transaction.newDemandQueue.subscribe(function () {
            let demands = this._target();

            if (demands.length > 0) {
                _.each(demands, function (element, index, list) {
                    self.demand.addDemand(element);
                });
                this._target.removeAll();
            }
        });

        // Watch for new supplies from unreserving a demand that
        // was reserved with a 'real' supply
        this.demand.newSupplyQueue.subscribe(function () {
            let supplies = this._target();

            if (supplies.length > 0) {
                _.each(supplies, function (element, index, list) {
                    self.supply.addSupply(element);
                });
                this._target.removeAll();
            }
        });

        // Watch for new demands from unreserving a supply that
        // was reserved with a 'real' demand
        this.supply.newDemandQueue.subscribe(function () {
            let supplies = this._target();

            if (supplies.length > 0) {
                _.each(supplies, function (element, index, list) {
                    self.supply.addSupply(element);
                });
                this._target.removeAll();
            }
        });
    }
    refreshAll() {
        var supplyDeferred, demandDeferred, transactionDeferred, self;
        self = this;
        this.clearSelected();

        core.shell.viewModel.loading(true);

        // foundation tooltip leaves the tooltips dangling.  This manually removes them
        _.each($('.dispatcher .has-tip'), function (tip) { $('#' + $(tip).data('selector')).remove(); });
        supplyDeferred = this.supply.refresh(this.commandBar);
        demandDeferred = this.demand.refresh(this.commandBar);
        transactionDeferred = this.transaction.refresh(this.commandBar);

        $.when(supplyDeferred, demandDeferred, transactionDeferred).always(function () {
            self.updateUnsavedChanges();
            if (self.transaction.data().length === 0) {
                core.shell.viewModel.loading(false);
            }
        });

        this.commandBar.searching(false);
    }

    assign(supply, demand) {
        let existingVoidedTransaction = null;

        this.supply.removeSupply(supply);
        this.demand.removeDemand(demand);
        // make sure it doesn't already exist as a voided one
        existingVoidedTransaction = this.transaction.findVoided(supply, demand);
        if (existingVoidedTransaction) {
            existingVoidedTransaction.restoreVoided();
        } else {
            this.transaction.addTransaction(supply, demand);
        }
        this.clearSelected();
    }

    clearSelected() {
        this.supply.selected(null);
        this.demand.selected(null);
        this.transaction.selectedSupply(null);
        this.transaction.selectedDemand(null);
    }

    getCommandQueue() {
        return {
            breakTransactions: _.map(_.filter(this.transaction.data(), function (item) {
                        return item.isVoided();
                    }),
                function (txn) {
                    return { id: txn.id, txLock: txn.txLock };
                }),
            unreserveDemand: _.map(
                _.union(
                    _.filter(this.demand.data(), function (item) {
                        return item.isUnreserved();
                    }),
                    _.filter(this.transaction.data(), function (transaction) {
                        return transaction.demand.isUnreserved();
                    })
                ),
                function (obj) {
                    let retVal;
                    if (obj.demand) {
                        retVal = { id: obj.demand.id, txLock: obj.demand.txLock};
                    }else {
                        retVal = { id: obj.id, txLock: obj.txLock };
                    }
                    return retVal;
                }),
            unreserveSupply: _.map(
                _.union(
                    _.filter(this.supply.data(), function (item) {
                        return item.isUnreserved();
                    }),
                    _.filter(this.transaction.data(), function (transaction) {
                        return transaction.supply.isUnreserved();
                    })
                ),
                function (obj) {
                    let retVal;
                    if (obj.supply) {
                        retVal = { id: obj.supply.id, txLock: obj.supply.txLock};
                    }else {
                        retVal = { id: obj.id, txLock: obj.txLock };
                    }
                    return retVal;
                }),
            reserveDemand: _.map(
                 _.filter(
                    _.flatten([this.demand.data(), _.map(this.transaction.data(), function (txn) { return txn.demand.original; })]),
                    function (item) {
                        return item.isReserved && item.isReserved();
                    }),
                function (demand) {
                    return {
                        id: demand.id,
                        txLock: demand.txLock,
                        supplyId: demand.reservedBy ? demand.reservedBy.id : null,
                        supplyTxLock: demand.reservedBy ? demand.reservedBy.txLock : null
                    };
                }),
            reserveSupply: _.map(
                 _.filter(
                    _.flatten([this.supply.data(), _.map(this.transaction.data(), function (txn) { return txn.supply.original; })]),
                    function (item) {
                        return item.isReserved && item.isReserved();
                    }),
                function (supply) {
                    return {
                        id: supply.id,
                        txLock: supply.txLock,
                        demandId: supply.reservedBy ? supply.reservedBy.id : null,
                        demandTxLock: supply.reservedBy ? supply.reservedBy.txLock : null
                    };
                }),
            addTransactions: _.map(_.filter(this.transaction.data(), function (item) {
                        return item.isNew();
                    }),
                function (txn) {
                    return {
                        supplyId: txn.supply.id,
                        supplyTxLock: txn.supply.original.txLock,
                        demandId: txn.demand.id,
                        demandTxLock: txn.demand.original.txLock,
                        contract: txn.contractAssignment() ? txn.contractAssignment().contract.id : null,
                        contractLocation: txn.contractAssignment() ? txn.contractAssignment().location.id : null,
                        contractClassification: txn.contractAssignment() ? txn.contractAssignment().classification.id : null,
                        balancingClassification: txn.balancingAssignment() ? txn.balancingAssignment().classification.id : null,
                        balancingLocation: txn.balancingAssignment() ? txn.balancingAssignment().location.id : null,
                        receivingDateTimeMin: txn.balancingAssignment() ? +txn.balancingAssignment().receivingDateTimeMin : null,
                        receivingDateTimeMax: txn.balancingAssignment() ? +txn.balancingAssignment().receivingDateTimeMax : null,
                        price: txn.balancingAssignment() ? txn.balancingAssignment().price : null
                    };
                }),
            deleteDemand: _.map(_.filter(this.demand.data(), function (item) {
                        return item.isDeleted();
                    }),
                function (demand) {
                    return { id: demand.id, txLock: demand.txLock };
                }),
            deleteSupply: _.map(_.filter(this.supply.data(), function (item) {
                        return item.isDeleted();
                    }),
                function (supply) {
                    return { id: supply.id, txLock: supply.txLock };
                }),
            completeSupply: _.map(_.filter(this.transaction.data(), function (item) {
                        return item.isCompleted() && item.supply.id;
                    }),
                function (txn) {
                    return { id: txn.supply.id, txLock: txn.supply.txLock };
                }),
            completeDemand: _.map(_.filter(this.transaction.data(), function (item) {
                        return item.isCompleted() && item.demand.id;
                    }),
                function (txn) {
                    return { id: txn.demand.id, txLock: txn.demand.txLock };
                }),
            summary: function (newDelimiter) {
                let summaryString = '',
                    delimiter = '\n';

                if (newDelimiter) {
                    delimiter = newDelimiter;
                }
                if (this.deleteSupply.length > 0) {
                    summaryString += 'Deleted ' + this.deleteSupply.length + ' Supplies' + delimiter;
                }
                if (this.deleteDemand.length > 0) {
                    summaryString += 'Deleted ' + this.deleteDemand.length + ' Demands' + delimiter;
                }
                if (this.reserveSupply.length > 0) {
                    summaryString += 'Reserved ' + this.reserveSupply.length + ' Supplies' + delimiter;
                }
                if (this.reserveDemand.length > 0) {
                    summaryString += 'Reserved ' + this.reserveDemand.length + ' Demands' + delimiter;
                }
                if (this.unreserveSupply.length > 0) {
                    summaryString += 'Unreserved ' + this.unreserveSupply.length + ' Supplies' + delimiter;
                }
                if (this.unreserveDemand.length > 0) {
                    summaryString += 'Unreserved ' + this.unreserveDemand.length + ' Demands' + delimiter;
                }
                if (this.breakTransactions.length > 0) {
                    summaryString += 'Voided ' + this.breakTransactions.length + ' Assignments' + delimiter;
                }
                if (this.addTransactions.length > 0) {
                    summaryString += 'Created ' + this.addTransactions.length + ' Assignments' + delimiter;
                }
                if (this.completeSupply.length > 0) {
                    summaryString += 'Completed ' + this.completeSupply.length + ' Supplies' + delimiter;
                }
                if (this.completeDemand.length > 0) {
                    summaryString += 'Completed ' + this.completeDemand.length + ' Demands' + delimiter;
                }
                return summaryString;
            },
            hasChanges: function () {
                return this.deleteSupply.length > 0 ||
                    this.deleteDemand.length > 0 ||
                    this.reserveSupply.length > 0 ||
                    this.reserveDemand.length > 0 ||
                    this.unreserveSupply.length > 0 ||
                    this.unreserveDemand.length > 0 ||
                    this.breakTransactions.length > 0 ||
                    this.addTransactions.length > 0 ||
                    this.completeSupply.length > 0 ||
                    this.completeDemand.length > 0;
            }
        };
    }

    updateUnsavedChanges() {
        this.supplyChanges(_.filter(this.supply.data(), function (item) {
            return item.isModified();
        }).length);
        this.demandChanges(_.filter(this.demand.data(), function (item) {
            return item.isModified();
        }).length);
        this.transactionChanges(_.filter(this.transaction.data(), function (item) {
            return item.isModified() && !item.isVoided();
        }).length);
    }

    save() {
        // get unsaved transactions
        let self = this,
            commandQueue = self.getCommandQueue();

        if (commandQueue.hasChanges() && !self.saveInProgress) {
            self.saveInProgress = true;
            core.shell.viewModel.loading(true);
            core.services.webapi.dispatcher.save(commandQueue)
            .done(data => {
                self.lastSaved('Last Saved: ' + moment().format('hh:mmA'));
            })
            .fail(data => {
                let responseError = false;
                if (data.responseJSON === undefined) {
                    let jsonObj = JSON.parse(data.responseText);
                    if (jsonObj !== undefined) {
                        data.responseJSON = jsonObj;
                    }
                }
                _.forEach(data.responseJSON, error => {
                    if (error.severity === 'ERROR') {
                        responseError = true;
                        core.shell.growler.error(error.message);
                    } else if (error.severity === 'WARNING') {
                        core.shell.modal.message = error.message;
                        core.shell.growler.warning(error.message);
                    }
                });
                if (data.responseJSON === undefined) {
                    if (data.status !== 200) {
                        responseError = true;
                        core.shell.growler.error(data.status + ': ' + data.statusText);
                    }
                }
                if (!responseError) {
                    self.lastSaved('Last Saved: ' + moment().format('hh:mmA'));
                }
            })
            .always(data => {
                self.saveInProgress = false;
                core.shell.viewModel.loading(false);
                this.refreshAll();
            });
        } else if (!self.saveInProgress) {
            core.shell.growler.warning('Nothing to Save');
        }
    }

    discard() {
        let self = this,
            commandQueue = this.getCommandQueue();

        if (commandQueue.hasChanges()) {
            core.shell.modal.create({
                title: 'Are you sure?',
                message: 'You have unsaved changes!  Hit OK to Discard the changes and continue.\n' + commandQueue.summary('\n'),
                primaryButton: {
                    display: 'OK',
                    action: function (context) {
                        self.refreshAll();
                        return true;
                    }
                }
            });
        }
    }

    assignIfValid(supply, demand) {
        var self = this;
        return core.services.webapi.dispatcher.dispatchable().validate(supply, demand)
        .done(function (data) {
            self.assign(supply, demand);
        })
        .fail(data => {
            if (data.responseJSON === undefined) {
                let jsonObj = JSON.parse(data.responseText);
                if (jsonObj !== undefined) {
                    data.responseJSON = jsonObj;
                }
            }
            _.forEach(data.responseJSON, error => {
                if (error.severity === 'ERROR') {
                    core.shell.growler.error(error.message);
                } else if (error.severity === 'WARNING') {
                    core.shell.growler.warning(error.message);
                    self.assign(supply, demand);
                }
            });
            if (data.responseJSON === undefined) {
                if (data.status !== 200) {
                    core.shell.growler.error(data.status + ': ' + data.statusText);
                }
            }
        });
    }

    initContextMenu(data) {
        this.contextMenu.initChoices(data);
    }

    isImplicitReservation(supply, demand) {
        return (demand.receivingLocation.isBrokerLocation && supply.reservable) || (supply.shippingLocation.isBrokerLocation && demand.reservable);
    }

    reserveIfValid(supply, demand) {
        let self = this;

        return core.services.webapi.dispatcher.dispatchable().validate(supply, demand)
        .done(function (data) {
            if (supply.reservable) {
                supply.reserve();
                supply.reservedBy = demand;
                if (demand.transaction) {
                    // blank out original demand so that it doesn't get put back in the demand panel
                    demand.transaction.demand.original = null;
                    demand.transaction.void();
                } else {
                    self.demand.removeDemand(demand);
                }
            } else if (demand.reservable) {
                demand.reserve();
                demand.reservedBy = supply;
                if (supply.transaction) {
                    // blank out original supply so that it doesn't get put back in the supply panel
                    supply.transaction.supply.original = null;
                    supply.transaction.void();
                } else {
                    self.supply.removeSupply(supply);
                }
            }
            self.clearSelected();
        })
        .fail(data => {
            if (data.responseJSON === undefined) {
                let jsonObj = JSON.parse(data.responseText);
                if (jsonObj !== undefined) {
                    data.responseJSON = jsonObj;
                }
            }
            _.forEach(data.responseJSON, error => {
                if (error.severity === 'ERROR') {
                    core.shell.growler.error(error.message);
                } else if (error.severity === 'WARNING') {
                    core.shell.growler.warning(error.message);
                    self.assign(supply, demand);
                }
            });
            if (data.responseJSON === undefined) {
                if (data.status !== 200) {
                    core.shell.growler.error(data.status + ': ' + data.statusText);
                }
            }
        });
    }
}

module.exports = DispatcherViewModel;
