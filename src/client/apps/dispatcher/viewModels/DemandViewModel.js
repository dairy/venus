const core = require('core'),
      ko = require('knockout'),
      _ = require('lodash'),
      $ = require('jquery'),
      moment = require('moment'),
      DemandModel = require('../models/DemandModel'),
      ContextMenuViewModel = require('./ContextMenuViewModel');

class DemandViewModel {
    constructor(filter) {
        _.extend(this, {
            data: ko.observableArray(),
            selected: ko.observable(),
            newSupplyQueue: ko.observableArray(),
            tableMap: ko.observable({
                type: {
                    name: 'type',
                    attribute: 'availability.displayName',
                    secondarySort: ['receivingDateTimeMin', 'receivingLocation.name'],
                    sortable: true
                },
                receivingDateTime: {
                    name: 'date',
                    attribute: 'receivingDateTimeMin',
                    secondarySort: ['receivingLocation.name'],
                    sortable: true
                },
                location: {
                    name: 'location',
                    attribute: 'receivingLocation.name',
                    secondarySort: ['receivingDateTimeMin'],
                    sortable: true
                },
                dmd: {
                    name: 'dmd#',
                    attribute: 'id',
                    sortable: true
                },
                weight: {
                    name: 'weight',
                    attribute: 'weight',
                    sortable: false
                }
            }),
            // Bind knockout callbacks to current 'this' context.
            refresh: _.bind(this.refresh, this),
            onSelect: _.bind(this.onSelect, this),
            delete: _.bind(this.delete, this),
            prepareDemand: _.bind(this.prepareDemand, this),
            addDemand: _.bind(this.addDemand, this),
            sortColumn: _.bind(this.sortColumn, this)
        });
    }

    refresh(filter) {
        let self = this;

        return core.services.webapi.dispatcher.dispatchable().demand(filter)
        .done(data => {
            self.data(ko.utils.arrayMap(data, self.prepareDemand));
        })
        .fail(data => {
            _.forEach(data.responseJSON, error => {
                if (error.severity === 'ERROR') {
                    core.shell.growler.error(error.message);
                } else if (error.severity === 'WARNING') {
                    // We shouldn't receive any warnings here.
                }
            });
        });
    }

    prepareDemand(item) {
        let demand,
            self = this;

        if (item instanceof DemandModel) {
            demand = item;
        } else {
            demand = new DemandModel(item);
            if (item.broken) {
                demand.status.push('broken');
            }

            demand.status.subscribe(function () {
                if (demand.isDeleted()) {
                    self.delete(demand);
                } else if (demand.isReserved() && !demand.isCompleted()) {
                    self.reserve(demand);
                } else if (demand.isAssignedToContract()) {
                    self.assignToContract(demand);
                } else if (demand.isCompleted()) {
                    self.complete(demand);
                } else if (demand.isUnreserved()) {
                    self.unreserve(demand);
                }

                demand.changedEvent(true);
            });
        }
        demand.hasContextChoices(ContextMenuViewModel.getChoices(demand).length > 0);
        return demand;
    }

    addDemand(item) {
        let demand = this.prepareDemand(item);
        this.data.push(demand);

        return demand;
    }

    removeDemand(item) {
        this.data.remove(item);
    }

    onSelect(item) {
        if (this.selected() === item) {
            this.selected(null);
        } else {
            this.selected(item);
        }
    }

    delete(item) {
        if (this.selected() === item) {
            this.selected(null);
        }
    }

    reserve(item) {

    }

    unreserve(item) {
        if (item.reservedBy) {
            this.newSupplyQueue.push(item.reservedBy);
        }
    }

    complete(item) {
        if (this.selected() !== item) {
            this.selected(item);
        }
    }

    assignToContract(item) {
        if (this.selected() !== item) {
            this.selected(item);
        }
    }
    isSelected(demand) {
        return demand === this.selected();
    }

    sortColumn(column) {
        core.utilities.sorter.sortColumn(column, this.data);
    }
}

module.exports = DemandViewModel;
