const core = require('core'),
      ko = require('knockout'),
      _ = require('lodash'),
      moment = require('moment');

class CommandBarViewModel {
    constructor() {
        let self = this;

        _.extend(this, {
            startDate: '',
            endDate: '',
            formattedStartDate: ko.observable(),
            formattedEndDate: ko.observable(),
            allReceivingLocations: ko.observableArray(),
            allShippingLocations: ko.observableArray(),
            receivingLocations: ko.observableArray(),
            shippingLocations: ko.observableArray(),
            haulers: ko.observableArray(),
            availabilities: ko.observableArray([
                {
                    name: 'contract',
                    selected: ko.observable(true),
                    tooltip: 'Hide/Show partner contract posts'
                },
                {
                    name: 'external',
                    selected: ko.observable(true),
                    tooltip: 'Hide/Show all external posts'
                }
            ]),
            initRecevingLocations: core.services.webapi.dispatcher.locations().receiving().done(
                data => {
                    this.allReceivingLocations(data);
                }),
            initShippingLocations: core.services.webapi.dispatcher.locations().shipping().done(
                data => {
                    this.allShippingLocations(data);
                }),
            searching: ko.observable(false),

            // Bind knockout callbacks to current 'this' context.
            getFilterString: _.bind(this.getFilterString, this),
            search: _.bind(this.search, this),
            selectAvailability: _.bind(this.selectAvailability, this)
        });

        this.formattedStartDate.subscribe(function () {
            if (this._target()) {
                self.startDate = moment(this._target(), 'M/D/YYYY').startOf('day');
            }
        });

        this.formattedEndDate.subscribe(function () {
            if (this._target()) {
                self.endDate = moment(this._target(), 'M/D/YYYY').endOf('day');
            }
        });
    }

    search() {
        let self = this;
        if (self.validateDates()) {
            this.searching(true);
        }
    }

    selectAvailability(data, event) {
        let self = this;
        _.map(this.availabilities(), function (item) {
            if (item.name === data.name) {
                item.selected(!item.selected());
            }
        });
    }

    validateDates() {
        let self = this,
            isValid = true;

        if (!moment(self.formattedEndDate(), ['M/D/YYYY', 'M/D/YY'], true).isValid() || !moment(self.formattedStartDate(), ['M/D/YYYY', 'M/D/YY'], true).isValid()) {
            isValid = false;
            core.shell.modal.create({
                title: 'Invalid Date',
                message: 'Date must be a valid date in the M/D/YYYY format.  Hit OK to continue.',
                primaryButton: {
                    display: 'OK',
                    action: function (context) {
                        return true;
                    }
                },
                secondaryButton: {
                    disabled: true
                }
            });
        } else if (moment(self.formattedEndDate(), ['M/D/YYYY', 'M/D/YY'], true).isBefore(moment(self.formattedStartDate(), ['M/D/YYYY', 'M/D/YY'], true))) {
            isValid = false;
            core.shell.modal.create({
                title: 'Invalid Date Selection',
                message: 'End Date must be after Start Date.  Hit OK to continue.',
                primaryButton: {
                    display: 'OK',
                    action: function (context) {
                        return true;
                    }
                },
                secondaryButton: {
                    disabled: true
                }
            });
        }
        return isValid;
    }

    getFilterString() {
        let plainFilter = {},
            rcvLocationIds = null,
            shipLocationIds = null;

        // force dates to be in Central timezone which is what the server expects
        plainFilter.startDate = +moment(this.startDate.startOf('day').format('YYYY-MM-DD') + 'T00:00:00-06:00');
        plainFilter.endDate = +moment(this.endDate.endOf('day').format('YYYY-MM-DD') + 'T23:59:59-06:00');
        rcvLocationIds = _.pluck(this.receivingLocations(), 'id');
        if (rcvLocationIds.length > 0) {
            plainFilter.includeReceivingLocationIds = rcvLocationIds;
        }
        shipLocationIds = _.pluck(this.shippingLocations(), 'id');
        if (shipLocationIds.length > 0) {
            plainFilter.includeShippingLocationIds = shipLocationIds;
        }
        plainFilter.hideContractPartnerPosts = !_.where(this.availabilities(), {name: 'contract'})[0].selected();
        plainFilter.hideExternalLoads = !_.where(this.availabilities(), {name: 'external'})[0].selected();
        return JSON.stringify(plainFilter);
    }
}

module.exports = CommandBarViewModel;
