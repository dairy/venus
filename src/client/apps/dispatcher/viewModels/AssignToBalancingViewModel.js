const core = require('core'),
      _ = require('lodash'),
      ko = require('knockout'),
      moment = require('moment');

class AssignToBalancingViewModel {
    constructor() {
        var self = this;
        this.name = '';
        this.title = ko.observable('');
        this.message = ko.observable('');
        this.validationMessage = ko.observable(null);
        this.validationMessages = ko.observableArray();
        this.visible = ko.observable(false);
        this.isInternalSupply = ko.observable(false);
        this.locations = ko.observableArray();
        this.location = ko.observable();
        this.priceFo1 = '';
        this.priceFo2 = '';
        this.priceFo3 = '';
        this.priceFo4 = '';
        this.priceCa1 = '';
        this.priceCa2 = '';
        this.priceCa3 = '';
        this.priceCa4a = '';
        this.priceCa4b = '';
        this.receivingDateTimeMin = '';
        this.receivingDateTimeMax = '';
        this.receivingDateMin = ko.observable();
        this.receivingDateHourMin = ko.observable().extend({ timeEntry: 2 });
        this.receivingDateMinuteMin = ko.observable().extend({ timeEntry: 2 });
        this.receivingDateMax = ko.observable();
        this.receivingDateHourMax = ko.observable().extend({ timeEntry: 2 });
        this.receivingDateMinuteMax = ko.observable().extend({ timeEntry: 2 });
        this.price = ko.observable().extend({ numeric: 4 });
        // TODO replace with enum from server
        this.classifications = ko.observableArray([
            {id: -1, name: '-- Select --'},
            {id: 0, name: 'FOI'},
            {id: 1, name: 'FOII'},
            {id: 2, name: 'FOIII'},
            {id: 3, name: 'FOIV'},
            {id: 4, name: 'CA1'},
            {id: 5, name: 'CA2'},
            {id: 6, name: 'CA3'},
            {id: 7, name: 'CA4a'},
            {id: 8, name: 'CA4b'}
        ]);
        this.classification = ko.observable(this.classifications()[0]);
        this.primaryButton = ko.observable({ display: 'OK', action: function () { return true; }});
        this.secondaryButton = ko.observable({ display: 'Cancel', action: function () { return true; }});

        this.receivingDateMin.subscribe(function () {
            if (this._target()) {
                self.receivingDateTimeMin = moment(this._target(), 'M/D/YYYY');
            }
        });
        this.receivingDateHourMin.subscribe(function () {
            if (this._target()) {
                self.receivingDateTimeMin = moment(self.receivingDateTimeMin).hour(this._target());
            }
        });
        this.receivingDateMinuteMin.subscribe(function () {
            if (this._target()) {
                self.receivingDateTimeMin = moment(self.receivingDateTimeMin).minutes(this._target());
            }
        });
        this.receivingDateMax.subscribe(function () {
            if (this._target()) {
                self.receivingDateTimeMax = moment(this._target(), 'M/D/YYYY');
            }
        });
        this.receivingDateHourMax.subscribe(function () {
            if (this._target()) {
                self.receivingDateTimeMax = moment(self.receivingDateTimeMax).hour(this._target());
            }
        });
        this.receivingDateMinuteMax.subscribe(function () {
            if (this._target()) {
                self.receivingDateTimeMax = moment(self.receivingDateTimeMax).minutes(this._target());
            }
        });
        this.classification.subscribe(function () {
            if (this._target()) {
                if (this._target().id === 0) {
                    self.price(self.priceFo1);
                } else if (this._target().id === 1) {
                    self.price(self.priceFo2);
                } else if (this._target().id === 2) {
                    self.price(self.priceFo3);
                } else if (this._target().id === 3) {
                    self.price(self.priceFo4);
                } else if (this._target().id === 4) {
                    self.price(self.priceCa1);
                } else if (this._target().id === 5) {
                    self.price(self.priceCa2);
                } else if (this._target().id === 6) {
                    self.price(self.priceCa3);
                } else if (this._target().id === 7) {
                    self.price(self.priceCa4a);
                } else if (this._target().id === 8) {
                    self.price(self.priceCa4b);
                }
            }
        });
    }

    classifications() {
        let self = this;
        return self.classifications;
    }

    create(data) {
        let self = this;
        _.each(_.keys(self), function (key) {
            if (_.has(data, key)) {
                if (_.isFunction(self[key])) {
                    self[key](data[key]);
                } else {
                    self[key] = data[key];
                }
            }
        });
        self.visible(true);
    }

    isValid(supply) {
        var self = this,
           isValid = true;
        self.validationMessages.removeAll();
        if (!this.location()) {
            isValid = false;
            self.validationMessages.push('Balancing Location must be selected.');
        }
        if (this.classification().id < 0) {
            isValid = false;
            self.validationMessages.push('Classification must be selected.');
        }
        if ((!moment(this.receivingDateMin(), 'M/D/YYYY').isValid()) ||
            this.receivingDateHourMin() < 0 || this.receivingDateHourMin() > 23 ||
            this.receivingDateMinuteMin() < 0 || this.receivingDateMinuteMin() > 59) {
            isValid = false;
            self.validationMessages.push('Earliest Receiving Date Time is not valid.');
        }
        if ((!moment(this.receivingDateMax(), 'M/D/YYYY').isValid()) ||
            this.receivingDateHourMax() < 0 || this.receivingDateHourMax() > 23 ||
            this.receivingDateMinuteMax() < 0 || this.receivingDateMinuteMax() > 59) {
            isValid = false;
            self.validationMessages.push('Latest Receiving Date Time is not valid.');
        }
        if (this.receivingDateTimeMax.isBefore(this.receivingDateTimeMin)) {
            isValid = false;
            self.validationMessages.push('Latest Receiving Date Time must be after Earliest.');
        }
        if (!this.price()) {
            isValid = false;
            self.validationMessages.push('Demand Price is required.');
        }
        if (this.classification().id >= 0 && this.classification().id < 4) {
            if (this.price() < 25.0001 || this.price() > 199.9999) {
                isValid = false;
                self.validationMessages.push('The CME% for Federal Order Cream must be between 25.0001 and 199.9999.');
            }
        } else if (this.classification().id >= 4 && this.classification().id < 9) {
            if (this.price() < -24.9999 || this.price() > 24.9999) {
                isValid = false;
                self.validationMessages.push('The Price for California class Cream must be between -24.9999 and 24.9999.');
            }
        }
        return isValid;
    }

    destroy() {
        this.visible(false);
        this.validationMessage(null);
        this.validationMessages.removeAll();
        this.location(null);
        this.classification(this.classifications()[0]);
        this.price(null);
    }

    handlePrimary() {
        if (this.primaryButton().action(this)) {
            this.destroy();
        }
    }

    handleSecondary() {
        if (this.secondaryButton().action(this)) {
            this.destroy();
        }
    }
}

ko.extenders.numeric = function (target, precision) {
    // create a writable computed observable to intercept writes to our observable
    var result = ko.pureComputed( {
        read: target,  // always return the original observables value
        write: function (newValue) {
            var current = target(),
                valueToWrite = null,
                roundingMultiplier = Math.pow(10, precision),
                newValueAsNum = isNaN(newValue) ? null : parseFloat(+newValue);

            if (newValue) {
                valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;
            }
            // only write if it changed
            if (valueToWrite !== current) {
                target(valueToWrite);
            } else {
                // if the rounded value is the same, but a different value was written, force a notification for the current field
                if (newValue !== current) {
                    target.notifySubscribers(valueToWrite);
                }
            }
        }
    }).extend({ notify: 'always' });

    // initialize with current value to make sure it is rounded appropriately
    result(target());

    // return the new computed observable
    return result;
};

ko.extenders.timeEntry = function (target, precision) {
    // create a writable computed observable to intercept writes to our observable
    var result = ko.pureComputed( {
        read: target,  // always return the original observables value
        write: function (newValue) {
            var current = target(),
                paddedValue = isNaN(newValue) ? 0 : newValue.toString().replace(/^([0-9,a-f])$/, '0$1');

            // only write if it changed
            paddedValue = paddedValue.toString().substring(0, 2);
            if (paddedValue !== current) {
                target(paddedValue);
            }
        }
    }).extend({ notify: 'always' });

    // initialize with current value to make sure it is rounded appropriately
    result(target());

    // return the new computed observable
    return result;
};

module.exports = AssignToBalancingViewModel;
