/* Modules
 =========================================================================== */
const path                              = require('path');
const webpack                           = require('webpack');
const pkg                               = require('./package.json');
const util                              = require('util');
const BrowserSyncPlugin                 = require('browser-sync-webpack-plugin');
const ExtractTextPlugin                 = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin                 = require('html-webpack-plugin');


/* Paths
 =========================================================================== */
const clientPath                        = path.resolve(__dirname, './src/client');
const clientCorePath                    = path.resolve(clientPath, './core');
const clientShellPath                   = path.resolve(clientPath, './shell');
const clientAssetsPath                  = path.resolve(clientPath, './assets');
const clientScssPath                    = path.resolve(clientAssetsPath, './styles/scss');
const clientLibPath                     = path.resolve(clientPath, './lib');
const clientAppsPath                    = path.resolve(clientPath, './apps');
const clientBindingsPath                = path.resolve(clientPath, './bindings');
const distPath                          = path.resolve(__dirname, './dist');
const nodeModulesPath                   = path.resolve(__dirname, './node_modules');
const bowerComponentsPath               = path.resolve(__dirname, './bower_components');
const foundationPath                    = path.resolve(__dirname, './node_modules/foundation-sites/scss');
const cssBundleName                     = 'css/[name].css';
const jsBundleName                      = 'js/[name].js';


/* Entry Points
 =========================================================================== */
const mainEntry                         = './main.js';
const dispatcherApp                     = path.resolve(clientAppsPath, './dispatcher/main.js');


/* Loader Configurations
 =========================================================================== */
const sassLoaders = [
    'css-loader?sourceMap',
    'sass-loader?outputStyle=expanded&' +
    'includePaths[]=' + clientScssPath + '&' +
    'includePaths[]=' + foundationPath.constructor,
    'autoprefixer-loader?{ browsers:[\'last 2 version\'] }'
];


/* Main Configuration
 =========================================================================== */
module.exports = {
    cache: false,
    context: clientPath,
    entry: {
        app: mainEntry,
        vendor: pkg.vendors
    },
    output: {
        path: distPath,
        publicPath: '',
        filename: jsBundleName,
        chunkFilename: 'js/[chunkhash].js'
    },
    // devtool: 'source-map',
    module: {
        loaders: [
            { test: /\.js$/,    loader: 'babel-loader!eslint-loader', exclude: [ nodeModulesPath, clientLibPath, bowerComponentsPath ] },
            { test: /\.json$/,  loader: 'json-loader' },
            { test: /\.html$/,  loader: 'html-loader' },
            { test: /\.scss$/,  loader: ExtractTextPlugin.extract('style-loader', sassLoaders.join('!')) },
            { test : /\.(woff|woff2|ttf|otf|eot)$/,  loader: 'file-loader?emitPath=/&name=../fonts/[name].[ext]' },
            { test: /\.(png|jpg|jpeg|gif|svg)$/, loader: 'file-loader?emitPath=/&name=../images/[name].[ext]' },
            { test: /jquery\.js$/, loader: 'expose?$' },
            { test: /jquery\.js$/, loader: 'expose?jQuery' }
        ]
    },
    resolve: {
        extensions: [
            '',
            '.js',
            '.html',
            '.scss',
            '.css',
            '.json'
        ],
        modulesDirectories: [
            clientAppsPath,
            clientAssetsPath,
            clientBindingsPath,
            clientLibPath,
            clientPath,
            bowerComponentsPath,
            clientScssPath,
            foundationPath,
            nodeModulesPath
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: { baseDir: [ distPath ] }
        }),
        new HtmlWebpackPlugin({
            title: pkg.title,
            filename: 'index.html',
            template: path.resolve(clientPath, './index.tmpl.html')
        }),
        new ExtractTextPlugin(cssBundleName, {
            allChunks: false
        })
    ],
    eslint: {
        failOnWarning: false,
        failOnError: true,
        quiet: true
    }
};
