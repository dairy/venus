/* Modules
 --------------------------------------------------------------------------- */
var gulp = require('gulp'),
    config = require('./gulp.config.json'),
    sass = require('gulp-sass'),
    pkg = require('./package.json'),
    fs = require('fs'),
    runSequence = require('run-sequence'),
    banner = config.banner.join('\n'),
    jqueryCheck = config.jqueryCheck.join('\n'),
    jqueryVersionCheck = config.jqueryVersionCheck.join('\n'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    csscomb = require('gulp-csscomb'),
    replace = require('gulp-replace'),
    eslint = require('gulp-eslint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    del = require('del'),
    vinylPaths = require('vinyl-paths'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    notifier = require('node-notifier'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    header = require('gulp-header'),
    cp = require('child_process'),
    splat = '**/*',
    webpack = require('webpack'),
    WebpackDevServer = require('webpack-dev-server'),
    webpackConfig = require('./webpack.config.js'),
    gutil = require('gulp-util'),
    // create a single instance of the compiler to allow caching
    webpackCompiler;

/* Paths
 --------------------------------------------------------------------------- */


/* Tasks
 --------------------------------------------------------------------------- */
gulp.task('clean-dist', function () {
    return gulp.src([config.paths.dist.root], {read: false})
        .pipe(vinylPaths(del));
});

gulp.task('clean-site', function () {
    return gulp.src([config.paths.site.root], {read: false})
        .pipe(vinylPaths(del));
});

gulp.task('clean', ['clean-dist', 'clean-site'], function () {
    return gulp.src(config.paths.clean, {read: false})
        .pipe(vinylPaths(del));
});

gulp.task('compile-scss', function () {
    return gulp.src([config.paths.assets.scss + 'application.scss'])
        .pipe(rename('venus.scss'))
        .pipe(sourcemaps.init())
        .pipe(sass({style: 'expanded'}))
        .pipe(autoprefixer())
        .pipe(header(banner, {pkg: pkg} ))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.paths.dist.css))
        .pipe(gulp.dest(config.paths.site.css))
        .pipe(notify({
            'title': 'Compile CSS',
            'message': 'Compile CSS task complete!',
            'sound': 'Frog',
            'onLast': true,
            'wait': true
        }));
});

gulp.task('minify-css', function () {
    return gulp.src([
        config.paths.dist.css + splat,
        '!' + config.paths.dist.css + '*.map'
    ])
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.init())
        .pipe(minifycss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.paths.dist.css))
        .pipe(gulp.dest(config.paths.site.css));
});

gulp.task('compile-js', ['lint-js'], function () {
    return gulp.src([
        config.paths.core.js + '**/*.js',
        config.paths.docs.js + '**/*.js'
    ])
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(config.paths.dist.js))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.init())
        .pipe(uglify({preserveComments: 'some'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.paths.dist.js))
        .pipe(gulp.dest(config.paths.site.css))
        .pipe(notify({
            'title': 'Compile JavaScript',
            'message': 'Scripts task complete!',
            'sound': 'Frog',
            'onLast': true,
            'wait': true
        }));
});

gulp.task('lint-js', function () {
    return gulp.src([
        config.paths.core.js + splat,
        config.paths.docs.js + splat
    ])
        // eslint() attaches the lint output to the eslint property
        // of the file object so it can be used by other modules.
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failOnError last.
        .pipe(eslint.failOnError());
});


/* Watch
 --------------------------------------------------------------------------- */
gulp.task('watch', function () {
    gulp.src('gulpfile.js')
        .pipe(notify({
            title: 'Gulp Watch',
            message: 'I\'m watching you!',
            sound: 'Frog',
            onLast: true,
            wait: true
        }));

    // Watch .scss files
    gulp.watch([
            config.paths.core.scss + '**/*.scss',
            config.paths.docs.scss + '**/*.scss'
        ],
        ['compile-scss']
    );

    // Watch the magic files
    gulp.watch([
            '.gulpfile.js',
            '.eslintrc'
        ],
        ['watch']
    );

    // Watch .js files
    gulp.watch([
            config.paths.core.js + '**/*.js'
        ],
        ['lint-js']
    );
});

// Static Server + watching scss/html files
gulp.task('server', function () {
    browserSync.init({
        server: config.paths.site.root
    });

    // Resync browser on any _site changes
    gulp.watch(config.paths.site.root + '**/*').on('change', browserSync.reload);
});

/* Default Task
 --------------------------------------------------------------------------- */
gulp.task('default', ['webpack-dev-server']);

// Build and watch cycle (another option for development)
// Advantage: No server required, can run app from filesystem
// Disadvantage: Requests are not blocked until bundle is available,
//               can serve an old app on refresh
gulp.task('build-dev', ['webpack:build-dev'], function () {
    gulp.watch(['src/**/*'], ['webpack:build-dev']);
});

/* Production Build
 --------------------------------------------------------------------------- */
gulp.task('build', ['webpack:build']);

gulp.task('webpack:build', function (callback) {
    var _config = Object.create(require('./webpack.config.js'));

    _config.plugins = _config.plugins.concat(
        new webpack.DefinePlugin({
            'process.env': {
                // This has effect on the react lib size
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin()
    );

    // run webpack
    webpack(_config, function (err, stats) {
        if (err) {
            throw new util.PluginError('webpack:build', err);
        }

        gutil.log('[webpack:build]', stats.toString({
            colors: true
        }));

        callback();
    });
});

// modify some webpack config options
var myDevConfig = Object.create(webpackConfig);
myDevConfig.devtool = 'sourcemap';
myDevConfig.debug = true;

// create a single instance of the compiler to allow caching
webpackCompiler = webpack(myDevConfig);

gulp.task('webpack:build-dev', function(callback) {
    // run webpack
    webpackCompiler.run(function(err, stats) {
        if (err) {
            throw new gutil.PluginError('webpack:build-dev', err);
        }

        gutil.log('[webpack:build-dev]', stats.toString({
            colors: true
        }));

        if (stats.hasErrors()) {
            notifier.notify({
                title: 'Venus ERROR',
                message: 'Build failed'
            });
        } else {
            notifier.notify({
                title: 'Venus Build',
                message: 'Built successfully'
            });
        }

        callback();
    });
});

gulp.task('webpack-dev-server', function (callback) {
    // modify some webpack config options
    var config = Object.create(webpackConfig);

    config.devtool = 'eval';
    config.debug = true;

    // Start a webpack-dev-server
    new WebpackDevServer(webpack(config), {
        publicPath: '/' + config.output.publicPath,
        stats: {
            colors: true
        }
    }).listen(8080, 'localhost', function(err) {
            if (err) {

                throw new gutil.PluginError('webpack-dev-server', err);
            }

            gutil.log('[webpack-dev-server]', 'http://localhost:8080/webpack-dev-server/index.html');

            // keep the server alive or continue?
            // callback();
        });
});
